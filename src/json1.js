import axios from 'axios'
// import store from 'src/store'

const EnterpriseID = '1108608537'

/** @@Test local json */
// import json_Banner from 'jsondata/1108608537/GetBrandBanner.json' // NG(Module not found
// let json_Banner = require('jsondata/1108608537/GetBrandBanner.json') // NG!!
// console.log('json123_Banner: ', json_Banner); // @@


function MakeUrl(v1) {
	// const EnterpriseID = '1108608537'
	// const url = location.host+`/jsondata/${EnterpriseID}/${v1}.json`	// CORS policy
	const url = `jsondata/${EnterpriseID}/${v1}.json`
	// console.log('MakeUrl: ', url); // @@
	return url
}

async function FetchJSON(url) {
	/** @Remember: blocked by CORS(跨域error,只聽後端,前端有無皆沒差 */
/* 
	const body = null;
	const head = {
		"headers": {
			// "Content-Type": "application/x-www-form-urlencoded",
			'Content-Type': 'application/json',
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token",
			'Cache-Control': null,
			'X-Requested-With': null,
		}
	}
	let res1 = await axios.get(url, body, head)
 */
	let res1 = await axios.get(url).catch (() => {})

	// console.warn('Fetch_JSON: '+url, res1); // @@
	/** @Fix: 有些檔案會變字串 */
	if (res1) {
		const data1 = res1.data;
		return typeof data1 == 'string' ? JSON.parse(data1) : data1;
	} else {
		return [];
	}
	// return res1 ? res1.data : [];
}

/** @@Test 專供-抓取暫存測試用的json數據 */
async function GetJsonTest(_act) {
	const url = `test/${_act}.json`
		,data = await FetchJSON(url)
	// console.log('GetJsonTest-_act,data: ',_act ,data); // @@
	return data;
}
async function GetJsonData1(_act) {
	const url = MakeUrl(_act)
		,data = await FetchJSON(url)
	// console.log('GetJSON-_act,data: ',_act ,data); // @@
	return data;
}

// async function Init() {
// }

// Init();

async function FetchDrawMinute() {
	const url = `https://8012.jh8.tw/public/GetAppSysConfigOneEntry.ashx?EnterpriseID=${EnterpriseID}&AppSysName=AppDrawMinute`
		,res1 	= await axios.get(url).catch (() => {})
	// console.warn('FetchDrawMinute: '+url, res1); // @@
	let nMin = 0;
	if (res1.status == 200) {
		const str1 	= res1.data
			,v1 			= str1.substring(1+str1.lastIndexOf(":")).replace(/'/g, '')
		nMin = parseInt(v1);
	// console.log('FetchDrawMinute-v1,nMin: '+v1, nMin);	// @@
	}
	return nMin;
}


export {GetJsonTest, GetJsonData1, FetchDrawMinute}
