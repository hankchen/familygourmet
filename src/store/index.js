// vue basic lib
import Vue from 'vue'
import Vuex from 'vuex'
import VueCookies from 'vue-cookies'
import Qs from 'qs';
Vue.use(Vuex)
Vue.use(VueCookies)
Vue.use(Qs)
// 客製/第三方 lib
import md5 from 'md5'
import axios from 'axios'
import fetchData from '../libs/fetchData.js'

// import { getDistance } from 'geolib'
import routerKakar from 'src/appFami/router'
// import routerWuhulife from 'src/appWuhulife/router'
// import {GetJsonTest,GetJsonData1} from 'src/json1.js'	// 取得json數據
import {GetJsonData1} from 'src/json1.js'	// 取得json數據
import { deviseFunction, ensureDeviseSynced, deviseSetLoginInfo, setRedirectPage, goMOrder } from 'src/libs/deviseHelper.js'
import { showAlert, setLoading, makeid_num, removeEmojis, isNotLinePara } from 'src/libs/appHelper.js'
import { formatTime } from 'src/libs/timeHelper.js'
import { getMarketShopID } from 'src/libs/market.js'

// import { S_Obj,L_Obj,IsFn,IsMobile,ReplaceDate,GetDateDiff } from 'src/fun1.js'	// 共用fun在此!
import { S_Obj,L_Obj,IsFn,IsMobile,ReplaceDate } from 'src/fun1.js'	// 共用fun在此!


// 企業會員卡相關資訊(所屬門店、卡號、儲值金額、會員等級、優惠卷 ... etc)
function rawRemberData() {
  return {
    isLoaded: false,
    // 會員卡
    card_info: {},
    // 會員資料
    vip_info: {},
    // (可使用的) 優惠卷清單
    ticketList: [],
    // (尚未能使用的) 優惠卷清單
    notAvailableTickets: [],
    // (已轉贈的) 優惠卷清單
    gaveTicketList: [],
    // 會員購買交易資料(有購買品項與總金額)
    purchaseRecord: [],
    // 會員積點交易資料(使用紀錄)
    expiredPoints: [],
    // (單一)優惠卷明細資訊 e.g. 使用規則、條款、期限、適用範圍 ... etc
    ticketInfo: {
      // 規則代號
      currentTypeCode: '',
      currentRuleCode: '',
      // 使用 QRcode
      currentTicketGID: '',
      currentTicketQRcode: '',
      // 規則條款
      ruleData: {},
      // 適用商品
      usageFoods: [],
      // 適用門店
      usageShops: [],
    }
  }
}

// 企業公開資料相關
function rawPublicData() {
  return {
    // LOGO 圖檔
    logo: '',
    logoLoaded: false,
    // DB connect 位置
    connect: '9001',
    // 首頁
    index: {
      // 主畫面可滑動的 Banners
      slideBanners: [],
      // 固定的副 Banner
      subBanner: [],
    },
    // 品牌資訊(關於我們,使用者條款,常見問題 ... etc)
    brandInfo: [],
    // 最新消息
    news: { data: {}, types: {}, banners: {}, },
    // 菜單頁
    menu: {
      // 大類 (代號, 名稱, 清單)
      mainType: [], // [{ code: '', name: '',  list: {} }, { code: '', name: '',  list: {} }, ...]
      // 小類 (代號, 名稱, 清單)
      subType: [],  // [{ code: '', name: '',  list: {} }, { code: '', name: '',  list: {} }, ...]
    },
    // 門店資訊頁
    storeList: { store: [] },
    // 主企業號 ID & connect (用於線上點餐)
    MainEnterPriseID: '',
    MainConnect: '',
    myBookList: null, // 我的預約
    myBookStatusList: [], // 我的預約狀態 - S:已送出, P:處理中, Y:己確認, N:己取消
    currentProduct: {} // 目前商品
  }
}
// 商城共用資料相關
function rawMallData() {
  return {
    foodkind2:[], //大類
    foodkind:[],//中類全部,未分
    kind:[],  //次類,內有foods(次類中的品項)
    foodTaste:{}, //口味加料
    iv_foods:{},//小類下的所有品項
    iv_styles:{},//品項下的所有規格
    foodSpec:[],//規格
    foodSpecKind:[],//規格類別
    shipData:[],//收件人清單
    iv_spec:{},//品項規格
    orderstatus:{},//訂單狀態
    shopOrderTmp:{Total:0},//訂單主表
    orders:[], //訂單清單
    imgUrl: 'https://9001.jinher.com.tw/WebPos/images/Food/',
    nowItem:{},//當下所選品項
    spyIndex: 0,//規格index
    shopping_cart:[],//購物車
    foodQty:{},//品項計數
    kindQty:{},//品項類別計數
    isCarUI:false,
    Banner:[], //商城首頁橫幅
    Header:[], //推薦商品群組
    iv_currentItem:[], //所有商品
    foodInfo:{},//商品資料object
    foodFreight:{},//運費相關
    foodExpresst:{},//物流運費相關
    foodMeasurement:{},//材積相關
    reloadShopFn:null,
    kind2Index:0, //記下目前進入的大類
    mallScrollTop:0,
    sortPriceType:'',
    barcodeItem:[{ID:"T001",Name:"消費體驗高雄館-餐點消費",Price1:''},{ID:"T002",Name:"消費體驗高雄館-商品消費",Price1:''}],//由掃瞄生成的品項
    thirdPartyPay:{},
    checkKind_list:[],
    singleUnFinish:[],
    singleFinish:[],
    becomeApp:false,
    payStatus:{},//付款狀態
    ordersPage:{}, //訂單清單,分頁
    unOrdersPage:{}, //未付款訂單清單,分頁
    cvsShopInfo:{},//選好的超商info
    ShopID:'',//商城訂單預設門店
    ShopTrace:{},//物流詳情
    shipDataDefault:{isLoad:false},//收件人預設
  }
}
// 依據環境參數，來決定 API baseUrl 的值
const baseUrl 	= 'fig.jh8.tw'
	,Test8012 	= '8012test.jh8.tw'

const state = {
  appSite: 'fig', //'kakar', // 系統參數: 站台
  promise: null, // 保存promise
  // 是否在殼裡面(判斷能否使用殼相關接口的依據)
  isDeviseApp: false,
  // 啟動/關閉 『讀取中』的動畫效果
  isLoading: false,
  marketBanner:{},//暫存卡包企業首頁進商城主橫幅
  loadingMsg: '',
  // 顯示手動輸入 QRcode 表格
  showInputQRCode: false,
  // 客製的彈窗 (用於 alert / confirm)
  customModal: {
    type: '', text: '', confirmText: '', cancelText: '',
    // confirm note
    confirmNoteTitle: '', // 附註標題
    confirmNoteChoice: [], // 附註選項
    confirmNoteChoosed: '', // 附註選項已選
    confirmNoteRemark: '', // 附註選了其他要手key的說明
    confirmNoteRemarkPlaceHolder: '', // 其他原因的place holder
    confirmNoteWarnMsg: '', // 警告訊息
    confirmNoteText: '', // 附註標題(style2)
  },
  // CSS 全域參數(用於判斷 CSS 是否設定 -webkit-overflow-scrolling: touch)
  // true => touch,
  // false => auto (幾乎只用於首頁)
  cssTouch: true,
  lineLogin:{
    id:'1656657051',
    sc:'c4d1747504ad9525a6115d555effd395',
    token:'',
    verify:'',
    brand:'',
    EnterpriseID:'',
  },
  lineNotifyInfo:{},
  thirdKey:{
    line:'LINEloginID',
    fb:'FBloginID',
  },
  isPageName: '', //內頁名稱
  supportPay:["Scan2Pay","Newebpay","LINEpay_web","JkoPay_web","ecPay","TSPGpay"],
  searchURL:{},
  // API url
  api: {
    Url8012: `https://${Test8012}/public/AppDataVIP.ashx`,
    figUrl: `https://${baseUrl}/public/AppFamilyGourmet.ashx`,
    // 卡+ 主服務
    kakarUrl: `https://${baseUrl}/Public/AppNCWOneEntry.ashx`,
    // 企業的公開資料
    publicUrl: `https://${baseUrl}/public/newsocket.ashx`,
    // 企業的會員資料
    memberUrl: `https://${baseUrl}/public/AppDataVIP.ashx`,	// +token
    // 企業的會員消費點數資料
    // consumerUrl: `https://${baseUrl}/public/AppDataVIP.ashx`,
    // 預約資料
    bookUrl: `https://${baseUrl}/public/AppBooking.ashx`,
    // 預約的token資料
    tokenUrl: `https://${baseUrl}/public/AppFamilyGourmet.ashx`,
    // 圖片的路徑
    picUrl: `https://${baseUrl}`,
    // 取會員已登記的一代卡資料
    jgssgUrl: `https://${baseUrl}/public/jgssgvip.ashx`,
    // 會員資料存取,例:收件人存取
    vipUrl: `https://${baseUrl}/public/APPMemberOneEntry.ashx`,
  },
  // 基本資訊
  baseInfo: {
    // 英文常稱
    isFrom: 'figapp',
    // 企業號
    EnterpriseID: '1108608537',
    // FunctionID
    FunctionID: '450501', // 全餐functionid=450501
    // 是否接收推播訊息 (跟殼要)
    pushNotify: '1', // 預設要開啟, 1:開啟推播, 0:關閉推播
    // GPS 資訊 (跟殼要)
    gps: {},
    // 螢幕亮度值 (跟殼要, range 0 ~ 255 預設為中間值)
    brightness: '120',
    // 前端打包檔版本號 (跟殼要)
    WebVer: '',
    // 殼的裝置 (iOS / Android)
    AppOS: '',
    // 殼是否有新版本
    DeviseVersionIsDiff: false,
    // (給線上點餐用) 回到 卡+ 時導向到指定企業 app
    currentEnterPriseID: (VueCookies.get('currentEnterPriseID') || ''),
    // 企業公開資料相關 (五互只需要rawPublicData()的這些欄位)
    publicData: {
      // LOGO 圖檔
      logo: '',
      logoLoaded: false,
      advertise: {
        adData: {}
      },
      companyData: [], // 公司資料: 隱私權政策,關於我們...
      // 首頁
      index: {
        // 主畫面可滑動的 Banners
        slideBanners: [],
        // 固定的副 Banner
        subBanner: [],
        slideBannersUpdate: false, // 主橫幅的圖要立即更新
        subBannersUpdate: false,   // 副橫幅的圖要立即更新
      },
      AppName: '',
      // 最新消息
      news: { data: {}, types: {}, banners: {}, },
      // 門店資訊頁
      storeIsLoaded: false,
      storeList: { store: [] },
      // === 門店搜尋條件 === start
      storeSearch: {
        searchBrand: '全部品牌',
        searchZoneDisplay: '全部地區',
        searchZoneType: '',
        searchCity: '',
        searchArea: '',
        searchKeyWord: '',
        store2Store: false // 在store頁,透過footer再度到store頁
      },
      // === 門店搜尋條件 === end
      brandList:[],//當下所選品牌下的品項
      brandLink:[],//品牌下的社群
      brandNews:[],//品牌下的消息
      brandNewsOne: {},
      cloudsearch:[],//搜尋列
      brandListKind:[],//當下所選品牌下的品項,kind
      brandListDetail:[],//當下所選品牌下的品項,detail
    },
    // 企業會員卡相關資訊(所屬門店、卡號、儲值金額、會員等級、優惠卷 ... etc)
    memberData: {
      vip_infoIsLoaded: false,
      // 會員資料
      vip_info: {},
      // 會員卡
      card_info: {},
      // 已登記的一代卡資料
      registeredCards: [],
      // (可領用的) 優惠卷清單
      drawTicketList: [],
      // 會員儲值交易資料(使用紀錄)
      depositRecordIsLoaded: false,
      depositRecord: [],
      // 積分換卷的資料
      pointToTickets: [],
      // 年度總積點資料
      expiredPoints_section: [],
      // 會員積點交易資料(使用紀錄)
      expiredPointsIsLoaded: false,
      expiredPoints: [],
      // (可使用的) 優惠卷清單
      ticketList: [],
      // (尚未能使用的) 優惠卷清單
      notAvailableTickets: [],
      // (已轉贈的) 優惠卷清單
      gaveTicketList: [],
      // (單一)優惠卷明細資訊 e.g. 使用規則、條款、期限、適用範圍 ... etc
      ticketInfo: {
        // 規則代號
        currentTypeCode: '',
        currentRuleCode: '',
        // 使用 QRcode
        currentTicketGID: '',
        currentTicketQRcode: '',
        // 規則條款
        ruleData: {},
        // 適用商品
        usageFoods: [],
        // 適用門店
        usageShops: [],
      },
      // 會員購買交易資料(有購買品項與總金額)
      purchaseRecord: [],
      consumerPoints: [], 			// 全部消費點數契約清單
      consumerPointDetail: {}, 	// 單一筆契約消費點數紀錄
      oneOrderDetail: [], 			// 單一筆訂單購物清單
      rememberDates: [], 				// 重要的日子
      pushNotifys: [], 					// 推播記錄
      pushNotifyNoRead: 0, 			// 推播記錄-未讀
    }
  },
  // 簡訊認證設定 (需要從殼取)
  SMS_Config: {
    // 簡訊回傳的驗證碼
    reCode: "",
    // 每天最多簡訊次數 (預設為 5 次)
    APPSMSDayTimes: '5',
    // 每封簡訊發送間隔 (預設為 50 秒)
    AppSMSIntervalMin: '50',
    // 每日簡訊發送扣打 (用來紀錄已寄出幾封, 不得超過 APPSMSDayTimes 的值), 格式： yyyymmdd-times => e.g. 20190513-4 (05/13 已用四次)
    SMSSendQuota: '',
  },
  // 會員接口相關資訊
  member: {
    mac: IsMobile()?'':(VueCookies.get('kkMAC') || ''),
    Tokenkey: IsMobile()?'':(VueCookies.get('kkTokenkey') || ''),
    code: IsMobile()?'':(VueCookies.get('kkUserCode') || ''),
    pwd: '',
    //pwd: IsMobile()?'':(VueCookies.get('kkUserPwd')||''),
  },
  // 首頁上面的卡面搜尋 bar
  appFilter: '',
  // 企業 app 的點擊次數統計
  appClicks: VueCookies.get('kkAppClicks') || {},
  vipAppsLoaded: false,
  // 已綁定的企業 app 列表
  vipApps: [],
  // 已綁定的企業 user 資料
  vipUserData: [],
  // 目前的進入/操作的 app
  currentAppUser: VueCookies.get('kkAppUser') || {},
  // (卡卡)會員帳號資訊
  userData: VueCookies.get('kkUserData') || {},
  // 企業會員卡相關資訊(所屬門店、卡號、儲值金額、會員等級、優惠卷 ... etc)
  memberData: rawRemberData(),
  // 企業公開資料相關
  publicData: rawPublicData(),
   // 商城公開資料相關
  mallData: rawMallData(),

  // 卡片條碼 QRcode
  cardQRCode: { image: '', value: '', loaded: false },
  cardListScroll2: 0,
  fontSizeBase: 1
}

const mutations = {
  /** 暫存字體大小 */
  setFontSizeTemp(state, fontSize) {
    if (!fontSize || isNaN(fontSize) || parseFloat(fontSize)<=0) {
      fontSize = 1 // 預設1
    }
    let floatFontSize = parseFloat(fontSize)
    state.fontSizeBase = floatFontSize
  },
  /** 永久保存字體大小 */
  setFontSizeForever(state, fontSize) {
    if (!fontSize || isNaN(fontSize) || parseFloat(fontSize)<=0) {
      fontSize = 1 // 預設1
    }
    let floatFontSize = parseFloat(fontSize)
    state.fontSizeBase = floatFontSize
    // 存入cookie
    VueCookies.set('fontSize', floatFontSize)
    // 若在殼中 => 存入殼裡
    if (IsMobile()) {
      let setThing = `{"spName":"fontSize", "spValue": ${ floatFontSize }}`
      deviseFunction('SetSP', setThing, '')
      // console.log('===> 已存入殼裡 =>' + setThing)
    }
  },
  // 存入系統參數：站台 & 基礎參數
  setAppSite(state, site) {
    state.appSite = site
    // 如果 site === 'wuhulife' , 要修改對應的 baseInfo.isFrom, baseInfo.EnterpriseID
    // if (site === 'wuhulife' ) state.baseInfo.isFrom = 'wuhulifeapp'
    // if (site === 'wuhulife' ) state.baseInfo.FunctionID = '450301'
    // if (site === 'wuhulife' ) state.baseInfo.EnterpriseID = '53239538'
    // if (site === 'wuhulife' ) state.baseInfo.AppName = '龍海就是消費'
  },
  // 存入當下進入的企業 app 資料
  setCurrentAppUser(state, app) { VueCookies.set('kkAppUser', app); state.currentAppUser = app },
  // 清空當下進入的企業 app 資料
  clearCurrentAppUser(state) { VueCookies.remove('kkAppUser'); state.currentAppUser = {} },
  // 存入企業 app 列表的點擊紀錄 (整個 object)
  saveAppClicks(state, clicks) { state.appClicks = clicks },
  // 存入企業 app 列表的點擊紀錄 ( EnterpriseID += 1 )
  setAppClicks(state, eID) {
    state.appClicks[eID] = state.appClicks[eID] || 0
    state.appClicks[eID] += 1
    // vue 的雷： object & array 的變化，需要用下面方式才會觸發 computed / getters
    // ref: https://github.com/vuejs/vuex/issues/1311
    Vue.set(state.appClicks, eID, state.appClicks[eID])
    // 存入 cookies
    VueCookies.set('kkAppClicks', state.appClicks)
    // 存入殼裡
    deviseFunction('SetSP', `{"spName":"appClicks", "spValue": ${ JSON.stringify(state.appClicks) }}`, '')
  },
  // 存入會員擁有的企業 app 使用者資料 (重複的就刷新點數資訊, 卡片資訊)
  setVipUserData(state, app) {
    const userData = state.vipUserData.find(item => item.EnterPriseID === app.EnterPriseID)
    const notFound = userData === undefined

    if (notFound) {
      state.vipUserData.push(app)
    }
    if (!notFound) {
      userData.cardPoint = app.cardPoint
      userData.CardTypeCode = app.CardTypeCode
      userData.CardTypeName = app.CardTypeName
      userData.CardFacePhoto = app.CardFacePhoto
    }
  },
  setVipUserLoaded() { state.vipAppsLoaded = true },
  // 設定 cssTouch
  setCssTouch(state, status){ state.cssTouch = status },
  // 設定 商城目前scrollTop
  setMallScrollTop(state, value){ state.mallData.mallScrollTop = value },
  // 設定是否在殼裡面
  setIsDeviseApp(state, status) { state.isDeviseApp = status },
  //暫存卡包企業首頁進商城主橫幅
  setMarketBanner(state, data) { state.marketBanner = data },
  // 『讀取中...』 切換
  setLoading(state, data) {
    state.isLoading = data.status
	/* 追加顯示-載入中訊息 */
	var msg = ''
	switch (data.mType) {
	case 1:
		msg = '資料處理中，請稍候...'
		break;
	case 2:
		msg = '資料加載中，請稍候...'
		break;
	case 3:
		msg = '急速加載中...'
		break;
	case 4:
		msg = '努力搶券中，請稍候...'
		break;
	default:
	}
	state.loadingMsg = msg
  },
  // 『顯示手動輸入 QRcode 表格』切換
  setInputQRCode(state, status) { state.showInputQRCode = status },
  // 設定彈跳視窗的資料 (顯示 / 隱藏)
  setCustomModal(state, { type, text, cancelText, confirmText,
    confirmNoteTitle, confirmNoteChoice, confirmNoteChoosed, confirmNoteRemark,
    confirmNoteRemarkPlaceHolder, confirmNoteWarnMsg, confirmNoteText,
    resolve, reject }) {
    cancelText = cancelText || '取消'
    confirmText = confirmText || '確認'

    state.customModal = { type, text, cancelText, confirmText,
      confirmNoteTitle, confirmNoteChoice, confirmNoteChoosed, confirmNoteRemark,
      confirmNoteRemarkPlaceHolder, confirmNoteWarnMsg, confirmNoteText}

    if (resolve && reject) state.promise = { resolve, reject }
  },
  // 存入已綁定的企業 app 列表
  setVipApps(state, payload) { state.vipApps = payload },
  // 存入手機簡訊認證的設定
  setSMS_Config(state, data) { state.SMS_Config = Object.assign({}, state.SMS_Config, data) },
  // 設定簡訊驗證碼
  setSMSreCode(state, data) { state.SMS_Config.reCode = data },
  // 存入基本資訊
  setBaseInfo(state, data) { state.baseInfo = Object.assign({}, state.baseInfo, data) },
  // 更新會員資訊
  setUserData(state, data) { state.userData = data },
  // line login 會員資訊
  setLineLoginInfo(state, data) {
    state.lineLogin.token = data.token;
    state.lineLogin.verify = data.verify;

    state.member.isFromTD = data.verify.tdType;
    state.member.FromTDID = data.verify.sub;
    state.member.MT_NickName = removeEmojis(data.verify.name || '');
    state.member.FromTDEmail = data.verify.email;

  },
  /** @NoUse: 不需要,因setLoginInfo己全蓋... */
  // setLoginInfoRtAPIUrl(state, data){
    // state.userData.RtAPIUrl = data.RtAPIUrl || "";
  // },

  // 存入Mac (在沒有登入的狀態)
  setMacNoLogin(state, mac) {
    // 存入 store
    state.member.mac = mac
  },
  setLineLoginKey(state, data) {
    state.lineLogin[data.key] = data.value;
  },
  setSearchUrl(state, data) {
    //line驗證返回時
    state.searchURL[data.key] = data.value;
  }, 
  // 存入會員登入資訊
  setLoginInfo(state, payload) {
    // payload sample: {data: data, rememberMe: true, pwd: 'xxx'}
    const data = payload.data
    // console.log('存入會員登入資訊: ', data)
    // console.log('rememberMe, pwd => '+payload.rememberMe, payload.pwd)

    // 存入 cookies
    if (payload.rememberMe) {
			// console.log('===> is payload.rememberMe')
			// console.log('setLoginInfo ===> save to cookies')
			VueCookies.set('kkMAC', data.mac)
			VueCookies.set('kkTokenkey', data.Tokenkey)
			VueCookies.set('kkUserData', data)
			VueCookies.set('kkUserCode', data.Account)
			//if (payload.pwd) VueCookies.set('kkUserPwd', payload.pwd)
    }
    // 存入 store
    state.userData = data
    state.member.mac = data.mac
    state.member.Tokenkey = data.Tokenkey
    state.member.code = data.Account
    if (payload.pwd) state.member.pwd = payload.pwd

		/** @Only: 1108608537	享聚卡(全家國際餐飲 */
    // 依據登入參數導向指定頁面
    if (data.EnterPriseID === "1108608537") setRedirectPage()
  },
  // 清除企業 app 的會員資料
  clearAppMemberData(state) { state.memberData = rawRemberData() },
  // 清除企業 app 的公開資料
  clearAppPublicData(state) { state.publicData = rawPublicData() },
  // 會員登出
  setLogout(state) {
    VueCookies.remove('kkJWT')
    VueCookies.remove('kkMAC')
    VueCookies.remove('kkTokenkey')
    VueCookies.remove('kkUserData')
    VueCookies.remove('kkUserCode')
    VueCookies.remove('kkUserPwd')
    VueCookies.remove('kkUserJWT')

		L_Obj.del('userData');
	// L_Obj.clear();	// nono:有存loginForm

    state.vipApps = []
    state.userData = {}
    state.vipUserData = []
    // 清除會員基本資訊
    state.member.mac = ''
    state.member.Tokenkey = ''
    state.member.code = ''
    state.member.pwd = ''

    // === 此console不可以移除 === start
    // let msg123 = '===> state.member =>' + JSON.stringify(state.member)
    //   + '<=== cookie => '
    //   + 'tokenkey=>' + VueCookies.get('kkTokenkey')
    //   + '<= kkJWT =>' + VueCookies.get('kkJWT')
    //   + '<= kkMAC =>' + VueCookies.get('kkMAC')
    //   + '<= kkUserData =>' + VueCookies.get('kkUserData')
    //   + '<= kkUserCode =>' + VueCookies.get('kkUserCode')
    //   + '<= kkUserPwd =>' + VueCookies.get('kkUserPwd')
    //   + '<= kkUserJWT =>' + VueCookies.get('kkUserJWT')

    // console.log(msg123)
    // alert(msg123)
    // === 此console不可以移除 === end
  },
  // 存入企業 app 的 LOGO 資訊
  setAppLogo(state, data) { state.publicData.logoLoaded = true ;state.publicData.logo = data },
  // 存入企業 app 的 LOGO 資訊 - 五互
  setAppLogoWuhu(state, data) { state.baseInfo.publicData.logoLoaded = true ;state.baseInfo.publicData.logo = data },
  // 存入我的預約
  setMyBookList(state, data) {
    if (data) {
      // 手動加入readMore開闔欄位
      data.forEach((item)=>{
        item.readMore = false // 預設是闔
      })
    }
    state.baseInfo.publicData.myBookList = data
  },
  // 存入我的預約
  setMyBookStatusList(state, data) {
    state.baseInfo.publicData.myBookStatusList = data
  },
  // 存入企業 app 的 db connect 資訊
  setAppConnect(state, data) { const connect = data || '9001' ; state.publicData.connect = connect },
  // 存入首頁主畫面可滑動的 Banners - 全餐
  setAdvertise(state, data) {
    // console.log('setAdvertise ===> data =>', data)
    // === only for test === start
    // data = {"ADBanner":[],"SUBanner":[]}
    // === only for test === end
    state.baseInfo.publicData.advertise.adData = data
  },
  setCompanyData(state, data) { state.baseInfo.publicData.companyData = data },
  //存入品牌下的商品
  setBrandData(state, data) { state.baseInfo.publicData.brandList = data },
  //存入品牌下的商品,by kindid
  setBrandDataKind(state, data) { state.baseInfo.publicData.brandListKind = data },
  //存入品牌下的商品 by detailid
  setBrandDataDetail(state, data) { state.baseInfo.publicData.brandListDetail = data },

  //存入品牌下的社群
  setBrandLink(state, data) { state.baseInfo.publicData.brandLink = data},
  //存入品牌下的消息
  setBrandNews(state, data) { state.baseInfo.publicData.brandNews = data},
  //存入品牌下的某一筆消息
  setBrandNewsOne(state, data) {
    if (data && data.length>0) {
      state.baseInfo.publicData.brandNewsOne = data[0]
    }
  },
  //存入搜尋列設定
  setCloudsearch(state, data) { state.baseInfo.publicData.cloudsearch = data },
  // 存入首頁主畫面可滑動的 Banners - 五互
  setIndexSlideBannersWuhu(state, data) { state.baseInfo.publicData.index.slideBanners = data },
  // 存入首頁固定的副 Banner
  setIndexSubBanner(state, data) { state.publicData.index.subBanner = data },
  // 五互 - 存入首頁固定的副 Banner
  setIndexSubBannerWuhu(state, data) { state.baseInfo.publicData.index.subBanner = data },
  // 存入企業門店清單
  setStoreListData(state, payload) {
    const {stores, MainEnterPriseID, MainConnect} = payload
    state.publicData.MainEnterPriseID = MainEnterPriseID
    state.publicData.MainConnect = MainConnect
    state.publicData.storeList = stores
  },
  // 存入企業門店清單
  setStoreListDataWuhu(state, payload) {
    let {stores, storeMode, MainEnterPriseID, MainConnect} = payload
    state.publicData.MainEnterPriseID = MainEnterPriseID
    state.publicData.MainConnect = MainConnect
	// console.log('存入企業門店清單-storeMode: ', storeMode);	// @@

	if (storeMode.length && stores.store && stores.store.length) {
		stores.store = stores.store.map(one => {
			!one.Brand && console.error('Brand為空??: ', one);	// @@
			const mode = storeMode.find(m => m.ShopID === one.OrgCode)
			// console.log('setStoreListDataWuhu-mode: ', mode);	// @@
			one.Mode = mode ? JSON.parse(mode.Mode) : undefined;
			one.ShopID = mode ? mode.ShopID : undefined;
			return one;
		})
	} else {
		console.error('Error:微管雲未設門店清單', storeMode);
	}
	// console.log('存入企業門店清單-stores: ', stores);	// @@

    state.baseInfo.publicData.storeList = stores;
  },

  setStoreQueryBrand(state, payload) {
    state.baseInfo.publicData.storeSearch.searchBrand = payload
  },
  setStoreQueryZone(state, payload) {
    state.baseInfo.publicData.storeSearch.searchZoneDisplay = payload.searchZoneDisplay
    state.baseInfo.publicData.storeSearch.searchZoneType = payload.searchZoneType
    state.baseInfo.publicData.storeSearch.searchCity = payload.searchCity
    state.baseInfo.publicData.storeSearch.searchArea = payload.searchArea
  },
  setStoreQueryKeyWord(state, payload) {
    //console.log("payload>>>>",payload);
    state.baseInfo.publicData.storeSearch.searchKeyWord = payload
  },
  setStore2Store(state, payload) {
    state.baseInfo.publicData.storeSearch.store2Store = payload
  },
  // 主橫幅的圖片要即時更新
  setMainBannerUpdate(state, payload) {
    state.baseInfo.publicData.index.slideBannersUpdate = payload
  },
  // 副橫幅的圖片要即時更新
  setSubBannerUpdate(state, payload) {
    state.baseInfo.publicData.index.subBannersUpdate = payload
  },
  // 存入品牌資訊
  setBrandInfo(state, data) { if (data) state.publicData.brandInfo = data },
  // 存入最新消息的資料
  setNewsData(state, data) { if (!data.ErrorCode) state.publicData.news.data = data },
  // 存入最新消息的資料
  setNewsDataWuhu(state, data) { if (!data.ErrorCode) state.baseInfo.publicData.news.data = data },
  // 存入最新消息的分類
  setNewsTypes(state, data) { if (!data.ErrorCode) state.publicData.news.types = data },
  // 存入菜單的大類資料
  setMenuMainTypeListData(state, payload) { state.publicData.menu.mainType.find(item => {return item.code === payload.code}).list = payload.data},
  //存入全部小類下商品
  setFoodmarketData(state, data) {
    const foodCheck=(foods,isSetFood)=>{return foods.map(p_food=>{
      p_food.ID = p_food.ID || p_food.MainID;  //品項ID
      p_food.Price = p_food.CurPrice;    //原價
      if (isSetFood && !state.mallData.foodInfo[p_food.ID]) state.mallData.foodInfo[p_food.ID] = p_food;
      // p_food.Price1 = (p_food.Price1 != undefined?p_food.Price1:9999);  //售價

      return p_food;
    });}

    if (Array.isArray(data.foods)) {
      state.mallData.iv_foods[data.kindID] = foodCheck(data.foods,true);

      const findKind = (et) => et.ID == data.kindID;
      var kind_index = state.mallData.foodkind.findIndex(findKind);
      state.mallData.foodkind[kind_index]["foods"] = foodCheck(data.foods);
      // console.log("setFoodmarketData",data.kindID,state.mallData);
    }

  },
  setMenuMainTypeCodeName(state, data) {
    const noHaveData = (state.publicData.menu.mainType.find(item => {return item.code === data.code}) === undefined)
    if (noHaveData) state.publicData.menu.mainType = state.publicData.menu.mainType.concat( { code: data.code, name: data.name, list: [] } )
  },
  // 存入菜單的小類資料
  setMenuSubTypeListData(state, payload)  {state.publicData.menu.subType.find(item => {return item.code === payload.code}).list = payload.data},
  setMenuSubTypeCodeName(state, data) {
    const noHaveData = (state.publicData.menu.subType.find(item => {return item.code === data.code}) === undefined)
    if (noHaveData) state.publicData.menu.subType = state.publicData.menu.subType.concat( { code: data.code, name: data.name, list: [] } )
  },
  setCurrentProduct(state, data) {
    if (data && data.length>0) {
      state.publicData.currentProduct = data[0]
      state.isPageName = data[0].KindName // 分類中文名稱
    }
  },
  //退貨 / 退款原因表 //RefundReason
  setReasonWuhu(state, data) {
    data = data || [];
    if (Array.isArray(data)){
      const p_reason = data.filter(rea=>{ return rea.isAppSet == true});
      state.baseInfo.memberData.appReason = p_reason.map((reaCode,index)=>{
        return {code:'reason_'+index,text:reaCode.RefundReason || ''};
      });
    }else{
      state.baseInfo.memberData.appReason = [];
    }
  },
  // 存入會員卡資訊
  setMemberVipData(state, data) {
    state.memberData.vip_info = data.querydata_return_info.vip_info
    state.memberData.card_info = data.querydata_return_info.card_info
    state.memberData.pointToTickets = data.PointToTickets // 積分換卷的資料
    state.memberData.expiredPoints_section = data.ExpiredPoints_section // 年度總積點資料
  },
  // 存入會員卡資訊 - 五互
  setMemberVipDataWuhu(state, data) {
    // === only for test 不同等級會員 === start
    // 再累積288元 即可升級 享金 會員
    // data.querydata_return_info.card_info.CardTypeCode = 'A'
    // data.querydata_return_info.card_info.DifferenceCumulativeSales = 288
    // data.querydata_return_info.card_info.CumulativeSales = 600
    // data.querydata_return_info.card_info.CardTypeCumulativeSales = 888

    // 再累積988元 即可升級 享鑽 會員
    // data.querydata_return_info.card_info.CardTypeCode = 'B'
    // data.querydata_return_info.card_info.DifferenceCumulativeSales = 988
    // data.querydata_return_info.card_info.CumulativeSales = 900
    // data.querydata_return_info.card_info.CardTypeCumulativeSales = 1888

    // 再累積588元 就能延續一年 享鑽 會員
    // data.querydata_return_info.card_info.CardTypeCode = 'C'
    // data.querydata_return_info.card_info.DifferenceCumulativeSales = 588
    // data.querydata_return_info.card_info.CumulativeSales = 1300
    // data.querydata_return_info.card_info.CardTypeCumulativeSales = 1888

    // 恭喜您成功延續一年 享鑽 會員
    // data.querydata_return_info.card_info.CardTypeCode = 'C'
    // data.querydata_return_info.card_info.DifferenceCumulativeSales = 0
    // data.querydata_return_info.card_info.CumulativeSales = 1900
    // data.querydata_return_info.card_info.CardTypeCumulativeSales = 1888
    // === only for test 不同等級會員 === end

		/** @@Test 積分換券 */
		// let json1 = await GetJsonTest('PointToTickets')	// @@
		// data.PointToTickets = json1
    //
    if (data && data.querydata_return_info) delete data.querydata_return_info.vip_info.PassWord
    
    state.baseInfo.memberData.vip_info = data.querydata_return_info.vip_info
    state.baseInfo.memberData.card_info = data.querydata_return_info.card_info
    state.baseInfo.memberData.pointToTickets = data.PointToTickets // 積分換卷的資料
    //console.log("優惠券總數data>>",state.baseInfo.memberData.vip_info.TicketCount);
    // === only for test start ===
    // data.ExpiredPoints_section = [{
				// ShopID: "TX07",
				// ShopName: "香繼光測試機B",
				// Points: "68.00",
				// ExpiredDate: "2020-12-31"
			// }, {
				// ShopID: "TX07",
				// ShopName: "香繼光測試機B",
				// Points: "6.00",
				// ExpiredDate: "2021-12-31"
			// }
    // ]
    // === only for test end ===

    // === 排序 start ===
    let expiredPoints_section = data.ExpiredPoints_section
    if (expiredPoints_section && expiredPoints_section.length>0) {
      if (expiredPoints_section.length>1) {
        // 排序: 由小到大
        expiredPoints_section.sort(function (a, b) {
          if (a.ExpiredDate > b.ExpiredDate) {
            return 1
          } else {
            return -1
          }
        })
      }
    }
    // === 排序 end ===

    state.baseInfo.memberData.expiredPoints_section = expiredPoints_section // 年度總積點資料

    // 2020-06-23 add: 重要的日子
    state.baseInfo.memberData.rememberDates = data.VIP_RemberDate
  },
  //顯示Qrcode
  setShowQrcode(state, data) { state.ShowQrcodeFn = data },
  // 存入卡片條碼 QRcode
  setCardQRCodeData(state, data) { state.cardQRCode = data },
  // (香香雞專屬) 存入已登記的一代卡資料
  setMemberRegisteredCardsData(state, data) { state.baseInfo.memberData.registeredCards = data },
  // 存入會員的購買交易紀錄
  setMemberPurchaseRecordData(state, data) {
    state.baseInfo.memberData.purchaseRecord = data
  },
  // 存入可領用的優惠卷列表資訊
  setMemberDrawTicketListData(state, data) {
    state.baseInfo.memberData.drawTicketList = data
  },
  // 存入優惠卷列表資訊
  setMemberTicketListData(state, data) {state.memberData.ticketList = data},
  // 存入優惠卷列表資訊 - 五互
  setMemberTicketListDataWuhu(state, data) {
    state.baseInfo.memberData.ticketList = data
  },
  // 存入已贈送的優惠卷列表資訊
  setMemberGaveTicketListData(state, data) {state.memberData.gaveTicketList = data},
  // 存入已贈送的優惠卷列表資訊 - 五互
  setMemberGaveTicketListDataWuhu(state, data) {
    state.baseInfo.memberData.gaveTicketList = data
  },
  // 存入尚未能使用的優惠卷資料
  setMemberNotAvailableTickets(state, data) {state.memberData.notAvailableTickets = data},
  // 存入尚未能使用的優惠卷資料 - 五互
  setMemberNotAvailableTicketsWuhu(state, data) {
    state.baseInfo.memberData.notAvailableTickets = data
  },
  // 存入會員的積點紀錄
  setMemberExpiredPointsData(state, data) { state.memberData.expiredPoints = data },
  // 存入會員的儲值交易紀錄
  setMemberDepositRecordData(state, data) {
    state.baseInfo.memberData.depositRecord = data
  },
  // 存入會員的積點紀錄
  setMemberExpiredPointsDataWuhu(state, data) { state.baseInfo.memberData.expiredPoints = data },
  // 存入會員的消費點數紀錄
  setMemberConsumerPointsDataWuhu(state, data) { state.baseInfo.memberData.consumerPoints = data },
  // 存入會員的消費點數紀錄
  setMemberConsumerPointDetailWuhu(state, data) {
    if (data) {
      // 手動加入readMore開闔欄位
      data.forEach((item)=>{
        item.readMore = false // 預設是闔
        item.orderDetail = null // 該筆訂單的購物清單
      })
    }

    state.baseInfo.memberData.consumerPointDetail = data
  },
  // 存入會員的單筆訂單紀錄
  setOneOrderDetail(state, data) {
    if (data) {
      let currentItem = state.baseInfo.memberData.consumerPointDetail[data.arrayIndex]
      if (currentItem) {
        currentItem.orderDetail = data.orderDetail
      }
    }
  },
  // 存入優惠卷明細資訊 e.g. 規則條款、期限、適用範圍 ... etc
  setMemberTicketInfoData(state, data) {
    const ticket = state.baseInfo.memberData.ticketInfo
    // 規則代號
    if (data.currentTypeCode) ticket.currentTypeCode = data.currentTypeCode
    if (data.currentRuleCode) ticket.currentRuleCode = data.currentRuleCode
    // 使用規則、條款
    if (data.ruleData)   ticket.ruleData = data.ruleData
    // 適用商品
    if (data.usageFoods) ticket.usageFoods = data.usageFoods
    // 適用門店
    if (data.usageShops) ticket.usageShops = data.usageShops
  },
  // 存入-推播消息-全部內容
  setPushNotifyData(state, data) {
    state.baseInfo.memberData.pushNotifys = data
  },
  // 存入-推播消息-總數
  setPushNotifyNum(state, num) {
    // state.baseInfo.memberData.pushNotifys = data
			// let arr1 = data.filter((o1) => {return !o1.isRead})
// console.log('存入會員的推播記錄: ', arr1);	// @@
    state.baseInfo.memberData.pushNotifyNoRead = num
  },
  // 存入優惠卷條碼 QRcode
  setTicketQRCodeData(state, data) {
    state.baseInfo.memberData.ticketInfo.currentTicketGID    = data.gid
    state.baseInfo.memberData.ticketInfo.currentTicketQRcode = data.image
  },
  // 存入殼的版本是否需要更新
  SetDeviseVersionIsDiff(state, status) {
    const value = (status === '1') ? true : false
    state.baseInfo.DeviseVersionIsDiff = value
  },
  //商城訂單預設門店
  setMarketShopID(state, data) { state.mallData.ShopID = data },
   // 存入購物車
  setShopCart(state, data) { state.mallData.shopping_cart = data },
   // 新增購物車
  addShopCart(state, data) { state.mallData.shopping_cart.push(data) },
     // 付款方式清單
  setCheckKind(state, data) { state.mallData.checkKind_list = data.filter(e=>{return e.ID !='ecmfmeCvsPay' && e.ID !='ecCvsPay'}); },
  // 商城觸發UI更新用
  turnShopCart(state) {state.mallData.isCarUI = !state.mallData.isCarUI},
  // 商城分組商品更新
  groupFood(state, data) {state.mallData.home[data.fIndex]["type"] = data.GroupItems;},
   // 存入商品詳情頁
  setShopNowItem(state, data) {state.mallData.nowItem = data},
  // 商城存入小類商品
  setShopKindFoods(state, data) {state.mallData.kind[data.index]["foods"] = data.foods},
  // 商城存入訂單記錄
  setShopRecord(state, data) { state.mallData.orders = data },
  // 商城存入訂單記錄,索引特定index
  setShopRecordIndex(state, data) { state.mallData.orders[data.index] = data.Obj },
  // 商城存入訂單記錄,分頁
  setShopRecordPage(state, data) { state.mallData.ordersPage = data },
  // 商城存入訂單記錄,分頁
  setShopUndRecordPage(state, data) { state.mallData.unOrdersPage = data },
  //付款狀態
  setPayStatus(state, data) {
    if(Array.isArray(data)){
      data.map((p_status)=>{
        const p_key =p_status.id.toString();
        state.mallData.payStatus[p_key] = p_status.text;
      });

    }
  },
  // 商城小類頁品項排序
  setSortPriceType(state, data) {state.mallData.sortPriceType = data},
  // 商城小類頁scrollListener
  setShoplistenELe(state, data) {state.mallData.listenEL = data},
  // 存入單筆未完成訂單記錄
  setSingleUnFinish(state, data) {
    state.mallData.singleUnFinish = data
  },
  // 存入單筆已經完成訂單記錄
  setSingleFinish(state, data) {
    state.mallData.singleFinish = data
  },
  // 存入預設通訊錄key值
  setDefaultDelivery(state, data) {
    state.mallData.shipDataDefault = data
  },
  // 存入通訊錄
  setDeliveryList(state, data) {
    state.mallData.shipData = data
  },
  setShopTrace(state, data) {
    //物流詳情
    state.mallData.ShopTrace = data
  },
  // 取回單筆已經選好的超商info
  setCvsShopInfo(state, data) {
    state.mallData.cvsShopInfo = data;
  },
  setBecomeApp(state) {
    state.mallData.becomeApp = !state.mallData.becomeApp
  },
  // 商城清除暫存
  clsShopMall(state) {
    state.mallData.foodkind2 = [];  //大類
    state.mallData.foodkind = [];   //小類
    state.mallData.iv_foods = {}; //小類下的所有品項
    state.mallData.iv_styles = {}; //品項下的所有規格
    state.mallData.foodInfo = {}; //by foodID 品項基本資料
  },
  initQrCodeItem(state){
    //由掃瞄生成的品項
    state.mallData.barcodeItem = [{ID:"T001",Name:"消費體驗高雄館-餐點消費",Price1:''},{ID:"T002",Name:"消費體驗高雄館-商品消費",Price1:''}];
  },

}

const getters = {
  getBaseUrl : () => {
    return baseUrl
  },
  fontSizeBase: state => {
    let fontSize = state.fontSizeBase
    if (!fontSize || isNaN(fontSize) || parseFloat(fontSize)<=0) {
      fontSize = 1 // 預設1
    }
    return parseFloat(fontSize)
  },
  // 站台 router
  router: () => {//state => {
    // if (state.appSite === 'wuhulife') return routerWuhulife
    return routerKakar
  },
  isNotLinePara:() => {return isNotLinePara()},
  // 是否登入
  isLogin: state => {
    return !!(state.member.code && state.member.mac && state.member.Tokenkey);
  },
  // 卡卡的 requestBody
  kakarBody: state => {
    const {mac, code} = state.member
    const {isFrom, EnterpriseID, FunctionID} = state.baseInfo
    return { FunctionID, EnterpriseID, isFrom, mac, Account: code, }
  },
  // 企業 app 的 requestBody
  appBody: state => (body) => {
    const inputBody = ( body || {} )
    const {mac, isFrom, EnterPriseID} = state.currentAppUser
    const basicBody = {
      mac, isFrom,
      EnterpriseID: EnterPriseID,
      Account: state.member.code,
    }

    return Object.assign({}, basicBody, inputBody)
  },
  // 企業 app 的 requestBody
  appBodyWuhu: state => (body) => {
    const inputBody = ( body || {} )
    const {mac, Tokenkey, isFrom} = {
      mac: state.member.mac,
      Tokenkey: state.member.Tokenkey,
      isFrom: state.baseInfo.isFrom
    }
    const basicBody = {
      mac, isFrom, Tokenkey,
      EnterPriseID: state.baseInfo.EnterpriseID, // 注意2個P大小寫不同
      Account: state.member.code,
    }
    return Object.assign({}, basicBody, inputBody)
  },
  // 企業 app 的 requestBody
  appBodyWuhuEncode: state => (body, paramBody) => {
    const inputBody = ( body || {} )
    const {mac, Tokenkey, isFrom} = {
      mac: state.member.mac,
      Tokenkey: state.member.Tokenkey,
      isFrom: state.baseInfo.isFrom
    }

    // === param start ===
    const paramInputBody = (paramBody || {})
    let paramData = {
      Account: window.btoa(state.member.code)
    }
    let paramEndData = Object.assign({}, paramData, paramInputBody)
    // === param end ===

    const basicBody = {
      mac, isFrom, Tokenkey,
      EnterPriseID: state.baseInfo.EnterpriseID, // 注意2個P大小寫不同      
      param: paramEndData,
    }

    return Object.assign({}, basicBody, inputBody)
  },
  // 目前的企業列表 (使用 state.appFilter 篩選 + 依據點擊次數排序)
  currentAppUsers: state => {
    let appUsers = state.vipUserData
    // 依據點擊次數排序
    appUsers = appUsers.sort((a, b) => {
      const aClicks = state.appClicks[a.EnterPriseID] || 0
      const bClicks = state.appClicks[b.EnterPriseID] || 0
      if (aClicks > bClicks) return -1
      if (aClicks < bClicks) return 1
      if (aClicks === bClicks) return 0
    })
    // 使用 state.appFilter 篩選
    if (state.appFilter === '') return appUsers
    const eIDs = state.vipApps.filter(item => {
      const filter  = state.appFilter.trim().toLowerCase()
      const eID     = item.EnterpriseID.trim().toLowerCase()
      const isFrom  = item.isFrom.trim().toLowerCase()
      const AppName = item.AppName.trim().toLowerCase()
      return RegExp(filter).test(eID) || RegExp(filter).test(isFrom) || RegExp(filter).test(AppName)
    }).map(item => item.EnterpriseID)
    return state.vipUserData.filter(item => eIDs.find(eID => (eID === item.EnterPriseID)) )
  },
  currentAppUsersWuhu: state => {
    let appUsers = state.vipUserData.sort((a, b) => {
      if (a.EnterPriseID < b.EnterPriseID) {
        return 1;
      }
      if (a.EnterPriseID > b.EnterPriseID) {
        return -1;
      }
    })
    // 依據點擊次數排序
    // appUsers = appUsers.sort((a, b) => {
    //   const aClicks = state.appClicks[a.EnterPriseID] || 0
    //   const bClicks = state.appClicks[b.EnterPriseID] || 0
    //   if (aClicks > bClicks) return -1
    //   if (aClicks < bClicks) return 1
    //   if (aClicks === bClicks) return 0
    // })

    // 使用 state.appFilter 篩選
    if (state.appFilter === '') return appUsers
    const eIDs = state.vipApps.filter(item => {
      const filter  = state.appFilter.trim().toLowerCase()
      const eID     = item.EnterpriseID.trim().toLowerCase()
      const isFrom  = item.isFrom.trim().toLowerCase()
      const AppName = item.AppName.trim().toLowerCase()
      return RegExp(filter).test(eID) || RegExp(filter).test(isFrom) || RegExp(filter).test(AppName)
    }).map(item => item.EnterpriseID)
    return appUsers.filter(item => eIDs.find(eID => (eID === item.EnterPriseID)) )
  },
  // 目前的企業 App
  currentApp: state => {
    return state.vipApps.find(item => item.EnterpriseID === state.currentAppUser.EnterPriseID)
  },
  connect: (state) => {
    return state.publicData.connect
  },
  // 企業 app 的 memberApiUrl
  appMemberApiUrl: (state, getters) => {
    return getters.currentApp.apiip
  },
  // 企業 app 的 memberApiUrl - 五互
  // appMemberUrlWuhu: (state) => {
    // return state.api.memberUrl
  // },
  // 企業 app 的 預約資料 - 五互
  appBookUrlWuhu: (state) => {
    return state.api.bookUrl
  },
  // 企業 app 的 預約token資料 - 五互
  appTokenUrlWuhu: (state) => {
    return state.api.tokenUrl
  },
  // 企業 app 的 publicApiUrl
  appPublicApiUrl: (state, getters) => {
    return getters.currentApp.apiip.replace('/public/AppVIP.ashx', '/public/AppNCW.ashx')
  },
  // 靜態檔的 url
  srcUrl: (state, getters) => {
    return getters.currentApp.apiip.replace('/public/AppVIP.ashx', '')
  },

  // appVipUrl: (state) => {
    // //會員資料存取,例:收件人存取 by 20200225
    // return state.api.vipUrl
  // },
  // 取手機的螢幕亮度值
  deviseBrightness: state => {
    if (state.baseInfo.brightness <= 70) return 120
    return state.baseInfo.brightness
  },
  // 全餐廣告
  getAdvertise: (state) => {
    return state.baseInfo.publicData.advertise
  },
  //  五互[我的預約]歷史紀錄
  getMyBookList: (state) => {
    return state.baseInfo.publicData.myBookList
  },
  //  五互[我的預約]狀態清單
  getMyBookStatusList: (state) => {
    return state.baseInfo.publicData.myBookStatusList
  }, //,
  getLineTk: (state) => {
    const p_head = { "headers": { "Content-Type": "application/x-www-form-urlencoded;" } }
		const hostN = window.location.hostname == 'localhost'?'localhost:8080':window.location.hostname;
		const redirect_name = window.location.protocol + "//" + hostN + window.location.pathname;
		const p_token = {
				grant_type: "authorization_code",
				code: state.code,
				redirect_uri: redirect_name,
				client_id: state.lineLogin.id,
				client_secret: state.lineLogin.sc }
    return Object.assign({}, {head:p_head,tokenObj:p_token})

  }
  // getCustomModal: (state) => {
  //   return state.customModal
  // }
}

const actions = {
  /* 卡卡資料相關 */
  // init 時執行所需的 action
  async fetchInitData() {
    // 1. 跟殼取資料
    await ensureDeviseSynced()
  },
  // (卡卡) 確認會員帳號是否未註冊
  async ensureMemberCodeNotRegistered({getters}, account) {
    const url = state.api.figUrl
    const body = getters.appBodyWuhu({
      act: 'memberchk',
      Account: account
    })
    const head = getters.requestHead
    const data = await fetchData({url, body, head})

    return new Promise(resolve => resolve(data))
  },
  // 使用積分換卷(後續邏輯由 call 此 action 的人處理)
  async ensurePointExchangeTicket({state, getters}, ticket) {
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({
      act: "PointExchangeTicket",
      OpenID: state.member.code,
      TicketCount: "1",
      TicketTypeCode: ticket.TicketTypeCode,
      Points: ticket.Points
    })

    const data = await fetchData({url, body})
	return !data.error ? data : data.detail;
  },
  // 優惠卷領用
  async ensureFetchDrawTicket({state, getters}, DrawGID) {
    // setLoading(true, 4);		// 改在接口
    const url = state.api.memberUrl
			,now1 	= formatTime(new Date(), 'YYYYMMDDHHmm')
		// console.log('優惠卷領用-now1: '+now1, DrawGID);	// @@
    const body = getters.appBodyWuhu({
      act: "GetDrawTicket",
      DrawGID: DrawGID,
      actTime: now1
    })

    const data = await fetchData({url, body})
    // setLoading(false);		// 改在接口
	return !data.error ? data : data.detail;
  },
  // (卡卡) 會員註冊
  async ensureMemberRegister({state}, payload){
    const url = state.api.figUrl
    const body = Object.assign({}, {
      act: "memberreg",
      "isFrom": state.baseInfo.isFrom,
      "EnterpriseID": state.baseInfo.EnterpriseID
    }, payload)
    const head = { "headers": { "Content-Type": "application/x-www-form-urlencoded" } }
    const {data} = await axios.post( url, body, head )
    return new Promise(resolve => resolve(data))
  },
  // LineLogin v2.1,主企業kakar
	async memberLineLogin_v2({state,commit},formData) {
    if (!formData) return;
    const {EnterpriseID, path, brand} = formData;
    if (!EnterpriseID || path === undefined || brand === undefined) return;
    setLoading(true)
    const hostN = window.location.hostname == 'localhost'?'localhost:8080':window.location.hostname;
		const redirect_name = window.location.protocol + "//" + hostN + window.location.pathname;
    const redirect_uri = encodeURIComponent(redirect_name);
    const url = "https://surl.jh8.tw/AppShortURL.ashx"
    const surlID = makeid_num(6);
		const head = { "headers": { "Content-Type": "application/x-www-form-urlencoded" } }
		const body = { act: "SetUrl",
									EnterPriseID: EnterpriseID,
									BackData: JSON.stringify({
										nonce:surlID,
                    EnterpriseID:EnterpriseID,
                    path:path,
                    brand:brand,
                  }),
									QrCodeType: "2",//1:url直接轉址,2:只運行 BackData
									ExpiredType: "1" //1:限1次,0:無限,2:限3天
								};
    const {data} = await axios.post( url, body, head )
    commit('setLoading', false)
    if (data.ErrorCode != '0') return showAlert(data.ErrorMsg || "")
		var p_state = (data.Remark || '');
		var lineUrl = `https://access.line.me/oauth2/v2.1/authorize?response_type=code&client_id=${state.lineLogin.id}&redirect_uri=${redirect_uri}&state=${p_state}&scope=openid%20profile%20email&nonce=${surlID}`;

		window.open(lineUrl, "_self");
	},
  // 第三方登入,以LINE log in為主
	async memberThirdCheck({dispatch,commit,getters}, formData) {
		setLoading(true)
		//短網址取得nonce(linelogin by jinher check)
		await axios.post( `https://surl.jh8.tw/${formData.state}`)
		.then((res) => {
			commit('setLoading', false);
			const dataSurl = res.data;
			if (!dataSurl.nonce) {
				return //showAlert(dataSurl.ErrorMsg || "error")
			}
      dispatch('thirdLineKey',dataSurl);
      if (!dataSurl.EnterpriseID || !dataSurl.path || getters.isLogin) return;
      dispatch('LineThirdToken_v2', Object.assign({}, formData, dataSurl ))
      
		}).catch( () => {
			commit('setLoading', false)
		})

	},
  thirdLineKey({commit}, dataSurl) {
    if (!dataSurl || !dataSurl.EnterpriseID || !dataSurl.path) return;
    if (dataSurl.brand) commit('setLineLoginKey', {key:"brand",value:dataSurl.brand}); ///品牌
    commit('setLineLoginKey', {key:"EnterpriseID",value:dataSurl.EnterpriseID});
    commit('setLineLoginKey', {key:"path",value:dataSurl.path}); //router path   
  },
  // 第三方登入,取line token v2.1
	async LineThirdToken_v2({dispatch,state}, formData) {
		setLoading(true)

    const {head,tokenObj} = getters.getLineTk(Object.assign({}, formData, state));

		//取linelogin token
    //accessToken    
		await axios.post( `https://api.line.me/oauth2/v2.1/token`, Qs.stringify(tokenObj),head )
		.then((res) => {
			setLoading(false);
			const p_data = res.data;
			if (!p_data.id_token) {return} //v2.1
			dispatch('LineThirdVerify', Object.assign({}, formData, { token: p_data.id_token } )) //v2.1

		}).catch( () => {
			setLoading(false);
		});

	},
  // 第三方登入,LINE verify v2.1
	async LineThirdVerify({dispatch,state, commit}, formData) {    
		setLoading(true)
		const head = { "headers": { "Content-Type": "application/x-www-form-urlencoded;" } }
		const p_token = { id_token: formData.token,
					nonce: formData.nonce,
					client_id: state.lineLogin.id }

		//取linelogin verify
		await axios.post( `https://api.line.me/oauth2/v2.1/verify`, Qs.stringify(p_token),head )
		.then(async (res) => {
			commit('setLoading', false);
			let p_data = res.data;      
			if (!p_data.sub) {return}
			p_data.tdType = "LINElogin";
			commit('setLineLoginInfo', {token:formData.token, verify:p_data});      
			await dispatch('memberThirdLogin', { sub: p_data.sub ,isFromTD: p_data.tdType})
      dispatch('thirdToPath');
		}).catch( () => {
			commit('setLoading', false)
		});
	},
  thirdToPath({state, commit, getters}) {
    if (!getters.isLogin) return;
    const {path,brand} = state.lineLogin;
    if (path == "cardShop") getters.router.push('/cardShop')//商城
    else if (path == "orders") goMOrder();// 導向我的訂餐
    else if (path == "shops") {
      //門市 by 品牌
      commit('setStoreQueryBrand', brand)
      getters.router.push('/cardStore')
    }
    else if (path == "cardCoupon") getters.router.push('/cardCoupon')//優惠券
    else if (path == "member")  getters.router.push('/cardMember')//會員
    else if (path == "memberQrCode") {
      getters.router.push('/card')
      setTimeout(() => {
        if (typeof state.ShowQrcodeFn === 'function') state.ShowQrcodeFn();//會員QRcode   
      }, 400)
      
    }    
    
    else getters.router.push('/card')   

    
  },
  async memberThirdLogin({state, commit, getters}, formData) {
		//第三方登入
    formData.isFromTD = formData.isFromTD || 'LINElogin'
    if (!formData.sub) return;
		const {isFrom, EnterpriseID} = state.baseInfo
		const url = state.api.memberUrl  //不用檢查是否測試帳號,不用oneentry
		const body = {
			isFrom, EnterpriseID,
			"act": "membersthirdlogin",
			"isFromTD": formData.isFromTD,//"LINElogin",
			"FromTDID": (formData.sub || formData.FromTDID)
		}

		const data 	= await fetchData({url, body})
		commit('setLoading', false)
		if (!data.error && Array.isArray(data) && data.length){
			let one	= data[0]			
      if (formData.isFromTD == 'LINElogin') one[state.thirdKey.line] = formData.sub
     
      one.FromTDID = formData.sub;
      commit('setLineLoginKey', {key:"isLoadReg",value:true});
			commit('setLoginInfo', {data: one, rememberMe: true})
			// => 存入會員資訊 (殼)
			deviseSetLoginInfo({type: 'set', data: one, rememberMe: true})
			// 取完後再導向首頁
			//if (!formData.isOnlyLoad) getters.router.push('/')
			// getters.router.push('/list')
		}else if(data.error && data.detail.ErrorCode === "5" && data.detail.Remark === ""){
			//未綁第三方(line login),或會員時,回應ErrorCode=5
      //showAlert("請註冊會員")
			getters.router.push({ path: '/register', query: { loginType: 'line' } })

		}
		//綁第三方,未綁會員時,應該不會發生
		//else if(data.error && data.detail.ErrorCode === "6" && data.detail.Remark === ""){}

	},
  async memberThirdRegister({state, commit}, account) {
		//第三方註冊
		setLoading(true)
		const {isFrom, EnterpriseID} = state.baseInfo
		const {isFromTD, FromTDID, MT_NickName, FromTDEmail} = state.member
		const url = state.api.memberUrl//memberbaseUrl
		const body = {
			isFrom, EnterpriseID,
			"act": "membersthirdreg",
			"isFromTD": isFromTD,
			"FromTDID":FromTDID,
			Account:account,
			FromTDNickName:MT_NickName,
			FromTDEmail:FromTDEmail,
		}
		const data 	= await fetchData({url, body})
		commit('setLoading', false)
		return new Promise(resolve => resolve(data))
	},
  // 撈沒有登入狀態的mac
  async getMacNoLogin({commit}) {
		commit('setMacNoLogin', S_Obj.mac)
  },
  // (卡卡) 會員登入
  async memberLogin({state, commit, getters}, formData) {
    setLoading(true);
    const {isFrom, EnterpriseID} = state.baseInfo
    let url = 'https://' + baseUrl + '/Public/AppDataVipOneEntry.ashx'
    const pwd = md5(formData.password)
    const body = {
      isFrom, EnterpriseID,
      "act": "login",
      "memberphone": formData.account,
      "memberPwd": pwd,
      "FunctionID":"450502",
    }
    let data 	= await fetchData({url, body})
		,one	= data[0]
		// console.log('memberLogin-data: ', one);	// @@

    // === token start ===
    // https://wuhuapp.jh8.tw/public/AppBookingOneEntry.ashx
    // url = getters.appTokenUrlWuhu
    // const tokenData = await fetchData({url, body})
    // === token end ===

		if (data.error) {
			setLoading(false);
			return showAlert(data.msg)
		}

		// IsMobile() && L_Obj.save('loginForm', formData);

		/** @Remember: 針對測試人員,登入資料需RtAPIUrl(8012test)重新登入,才能拿到測試站資料 */
		const RtAPIUrl = one.RtAPIUrl
		if (RtAPIUrl) {
			const isFromTest = /8012test/.test(RtAPIUrl)
			if (isFromTest) {
				const reqUrl = state.api.Url8012
				const head = { "headers": { "Content-Type": "application/x-www-form-urlencoded" } }
				data = await axios.post(reqUrl, body, head)
				// data = await fetchData({reqUrl, body})	// nono:必須一定要改由8012test
				const req1 = JSON.parse(data.data.Remark)[0]
				one = Object.assign({}, req1, {RtUrl: one.RtUrl, RtAPIUrl:one.RtAPIUrl})
				// console.log('member2Login-data: ', one);	// @@
			}
		}
		// L_Obj.save('userData', one);//明碼,不可,應加密
    if (IsMobile()) L_Obj.saveBa64('userData', one);

		setLoading(false);

		/** @NoUse: 不需要,因setLoginInfo己全蓋... */
    // commit('setLoginInfoRtAPIUrl', one)//為了區別是在正式 or 測試登入,取token (API service不同)

    // => 存入會員資訊 (cookie, store)
    commit('setLoginInfo', {data: one, rememberMe: formData.rememberMe, pwd})
    // => 存入會員資訊 (殼)
    deviseSetLoginInfo({type: 'set', data: one, rememberMe: true, pwd})
    // 取完後再導向首頁
    getters.router.push('/card') // Jerry說改成/card
  },
  // (卡卡) 會員登出
  memberLogout({commit, getters}) {
    // 清除 殼 裡的會員資料
    deviseSetLoginInfo({type: 'reset'})
    // 清除 state 裡的會員資料
    commit('setLogout')
    commit('setSearchUrl', {key:"code",value:""});
    commit('setSearchUrl', {key:"state",value:""});
    commit('setSearchUrl', {key:"linePara",value:""});
    commit('setSearchUrl', {key:"lineNofInfo",value:""});//LINE notify

    commit('setLineLoginKey', {key:"brand",value:""});
    commit('setLineLoginKey', {key:"isScan",value:true});
    commit('setLineLoginKey', {key:"RegId",value:""});
    window.history.replaceState(null, null, window.location.pathname);//有帶參數會拿掉
    // 導向登入頁
    getters.router.push('/login')
  },
  // 會員修改個人資訊
  async ensureMemberUpdateProfile({state, getters}, data){
    let url = 'https://' + baseUrl + '/Public/AppDataVIPOneEntry.ashx'
    let apiBody = Object.assign({}, data, {
      act: "memberupdate_wuhu",
      memberphone: state.member.code,
      FunctionID: "450501",
    })
    const body = getters.appBodyWuhu(apiBody)
    const update = await fetchData({url, body})

    return new Promise(resolve => resolve(update) )
  },

  // 享聚卡回娘家
  async ensureJgssgMemberCardBind({state, getters}, data){
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({
      act: "vipfig",
      cardNo: data.cardNO,
      validateCode: data.verifyNO
    })

    const update = await fetchData({url, body})

    // // === only for test === start
    // console.log('===>' + url )
    // console.log('===>', body )
    // let update = {
    //   ErrorCode: 0
    // }
    // // === only for test === end

    return new Promise(resolve => resolve(update) )
  },
  /**
    統一用這個先將品牌資料撈好
  */
  async fetchVipAppsByCheck({dispatch, commit}, enterpriseID) {
    await dispatch('fetchVipAppsIfNoData', enterpriseID)
    const findApp = state.vipUserData.find(app => app.EnterPriseID === enterpriseID)
    commit('setCurrentAppUser', findApp)
  },
  /**
    refresh時只呼叫卡包清單和單一品牌
  */
  async fetchVipAppsIfNoData({dispatch}, enterpriseID) {
    let brandList = state.vipUserData
    // 有資料表示為正常點按,不用重撈
    if (brandList && brandList.length>0) {
      // console.log('===> brandList > 0')
      new Promise(resolve => {resolve(true)})
    // refresh
    } else {
      // console.log('===> brandList < 0')
      await dispatch('fetchVipAppsOnlyList', enterpriseID)
    }
  },
  /**
    只呼叫卡包清單和單一品牌
  */
  async fetchVipAppsOnlyList({dispatch, getters, commit}, enterpriseID) {
    if (!getters.isLogin)		return

    const url = `https://${baseUrl}/Public/AppVipOneEntry.ashx`
    const body = Object.assign({}, getters.kakarBody, { act: 'GetKKVipAPP' } )
    const apps = await fetchData({url, body})
    if (apps.error) return

    await commit('setVipApps', apps)
    // 完成使用者的基本資料載入
    await commit('setVipUserLoaded')
    // 將"單一" app 的會員資料存入
    let app = await apps.find(curApp => {
      return curApp.EnterpriseID === enterpriseID
    })
    if (app) await dispatch('fetchVipAppData', {app: app})
  },
  // (卡卡) 取已綁定的企業 app 列表
  async fetchVipApps({dispatch, getters, commit}) {
    if (!getters.isLogin)		return
    const url = `https://${baseUrl}/Public/AppVipOneEntry.ashx`
    const body = Object.assign({}, getters.kakarBody, { act: 'GetKKVipAPP' } )
    const apps = await fetchData({url, body})

    if (apps.error) return

    // 存入已綁定的企業 app
    commit('setVipApps', apps)
    // 完成使用者的基本資料載入
    commit('setVipUserLoaded')
    // 將各 app 的會員資料存入
    apps.forEach(app => dispatch('fetchVipAppData', {app: app}) )
  },
  // (卡卡) 取綁定企業的基本資料
  async fetchVipAppData({state, getters, commit}, payload) {
    if (!getters.isLogin)		return

    const { app } = payload
    const { EnterpriseID, isFrom, mac } = app

    // 取會員的點數與卡資料
    const url = state.api.memberUrl
    const body = {
      act: "GetVipAndCardInfo",
      Account: state.member.code,
      mac, isFrom, EnterpriseID,
    }
    const vipCardInfo = await fetchData({url, body})
    if (vipCardInfo.error) return

    // 將取得的資料存入入 userData 裡面
    const ticketData = vipCardInfo.querydata_return_info.ticket_info
    const cardData   = vipCardInfo.querydata_return_info.card_info
    const vipData    = vipCardInfo.querydata_return_info.vip_info

    // 取企業會員的卡片圖檔
    let cardImg
    if (app.Dir && app.FileName) cardImg = 'https://' + baseUrl + app.Dir + app.FileName
    if (cardData.CardFacePhoto !== '') cardImg = app.apiip.replace('/public/AppVIP.ashx', '') + cardData.CardFacePhoto

    // 設定要存入的會員資料
    const userData = {
      Sex:           vipData.sex,
      mac:           app.mac,
      Name:          vipData.CnName,
      Email:         vipData.Email,
      isFrom:        app.isFrom,
      Address:       vipData.Addr,
      Mobile:        vipData.mobile,
      Account:       vipData.CardNO,
      Birthday:      vipData.BirthDay,
      ExpiredDate:   cardData.ExpiredDate,
      EnterPriseID:  app.EnterpriseID,
      CardID:        vipData.CardId,
      CardNO:        vipData.CardNO,
      cardGID:       cardData.GID,
      cardPoint:     parseInt(cardData.Points),
      cardTicket:    ticketData,
      CardTypeCode:  cardData.CardTypeCode,
      CardTypeName:  cardData.CardTypeName,
      CardFacePhoto: cardImg,
    }

    commit('setVipUserData', userData)
  },
  // 取付款狀態
  async fetchPayStatus({getters, commit, state}) {
    // 必須為登入狀態 && 有 currentAppUser
    if (!getters.isLogin) return
    //const url = state.currentAppUser.apiip || state.api.memberUrl;
    //let body = getters.appBodyWuhu({act: 'GetData',"DataName":"P_PayStatus"})
    const chkFetchData = (p_body)=>{
      const isSubsidiary = false;
      const url = state.api.memberUrl;
      const body = getters.appBodyWuhu(p_body)
      return {url,body,isSubsidiary}  
    }

    const data = await fetchData(chkFetchData({act: 'GetData',"DataName":"P_PayStatus"}));
    //const data = await fetchData({url, body})
    if (data.error) return
    commit('setPayStatus', data)
  },
  // 取會員資料 => 主要是點數給card/Member.vue和card/Point.vue
  async fetchVipAndCardInfo({getters, commit}) {
    // 必須為登入狀態 && 有 currentAppUser
    if (!getters.isLogin || state.currentAppUser.EnterPriseID === undefined) return

    const url = getters.appMemberApiUrl
    const body = getters.appBody({act: 'GetVipAndCardInfo'})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberVipData', data)
  },
  // 取會員資料 => 主要是點數給HomeMember.vue和HomePoint.vue
  async fetchVipAndCardInfoWuhu({getters, commit, state},p_data) {
    // console.log('fetchVipAndCardInfoWuhu ===> start')
    // 必須為登入狀態 && 有 currentAppUser
    if (!getters.isLogin || state.baseInfo.EnterpriseID === undefined) return

    const url = state.api.memberUrl
			,_act 	= 'GetVipAndCardInfo'
    const body = (p_data && p_data.type?getters.appBodyWuhu({act: _act,type:p_data.type}):getters.appBodyWuhu({act: _act}))
    const data = await fetchData({url, body})
    if (data.error) return showAlert(data.detail.ErrorMsg)
    state.baseInfo.memberData.vip_infoIsLoaded = true

    commit('setMemberVipDataWuhu', data)
  },
  //退款/退貨原因表 EC_RefundReason
  async fetchReasonWuhu({state, getters, commit}) {
    if (getters.isLogin === false || state.baseInfo.EnterpriseID === undefined) return
    if (Array.isArray(state.baseInfo.memberData.appReason) && state.baseInfo.memberData.appReason.length > 0) return //撈過跳過
    const url = state.api.memberUrl;
    const body = getters.appBodyWuhu({act: 'getAppReason',"FunctionID":"950201"})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setReasonWuhu', data)
  },
  // (卡卡) 取 currentAppUser 的相關資料
  async fetchAppUserData({dispatch, getters, commit, state}){
    // 必須為登入狀態 && 有 currentAppUser
    if (!getters.isLogin || state.currentAppUser.EnterPriseID === undefined) return

    const url = getters.appMemberApiUrl
    const body = getters.appBody({act: 'GetVipAndCardInfo'})
    const data = await fetchData({url, body})
    if (data.error) return

    return new Promise(resolve => {
      commit('setMemberVipData', data)
      // 取會員的消費紀錄
      dispatch('fetchMemberPurchaseRecordData')
      // 取會員的積點交易紀錄
      dispatch('fetchMemberExpiredPointsData')
      // 取優惠卷列表資訊
      dispatch('fetchMemberTicketListData')
      // 取會員已轉贈的優惠卷資訊
      dispatch('fetchMemberGaveTicketListData')
      // 取尚未能使用的優惠卷資料
      dispatch('fetchMemberNotAvailableTickets')

      resolve(true)
    })
  },
  // (卡卡) 取 currentAppUser 的相關資料 - 五戶
  async fetchAppUserDataWuhu({getters, commit, state}){
    // 必須為登入狀態 && 有 currentAppUser
    if (!getters.isLogin || state.baseInfo.EnterpriseID === undefined) return

    const url = state.api.picUrl + '/public/AppVIP.ashx' //getters.appMemberApiUrl
    const body = getters.appBodyWuhu({act: 'GetVipAndCardInfo'})
    const data = await fetchData({url, body})
    if (data.error) return

    return new Promise(resolve => {
      commit('setMemberVipData', data)
      // // 取會員的消費紀錄
      // dispatch('fetchMemberPurchaseRecordData')
      // // 取會員的積點交易紀錄
      // dispatch('fetchMemberExpiredPointsData')
      // // 取優惠卷列表資訊
      // dispatch('fetchMemberTicketListData')
      // // 取會員已轉贈的優惠卷資訊
      // dispatch('fetchMemberGaveTicketListData')
      // // 取尚未能使用的優惠卷資料
      // dispatch('fetchMemberNotAvailableTickets')

      resolve(true)
    })
  },
  // (卡卡) 取 cardQRcode
  async fetchCardQRCodeData({state, commit, getters}, GID) {
    // 先清空舊的 QRcode
    commit('setCardQRCodeData', { value: '', image: '', loaded: false } )
    const url = state.api.memberUrl
    const body = getters.appBody({act: 'GetVipCardQrCodeStr', GID})
    const data = await fetchData({url, body})
    if (data.error) return
    commit('setCardQRCodeData', { value: data.QrCode, image: data.QrCodeImg, loaded: true } )

  },
  // (龍海) 取 cardQRcode
  async fetchWuhuCardQRCodeData({state, commit, getters}, GID) {
    // 先清空舊的 QRcode
    commit('setCardQRCodeData', { value: '', image: '', loaded: false } )

    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({act: 'GetVipCardQrCodeStr', GID})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setCardQRCodeData', { value: data.QrCode, image: data.QrCodeImg, loaded: true } )

  },
  // (全餐) 取領券QRcode
  async fetchDrawTicketQRCode({state, getters}, GID) {

    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({act: 'GetDrawTicketQrCodeStr', GID})
    const data = await fetchData({url, body})
    if (data.error) return data.detail

    return data
  },
  // (卡卡) 進入企業 app
  async AccessInApp({commit}, userData) {
    // 存入企業號 (用於線上點餐，回到 卡+ 時正確導向到指定頁面)
    deviseFunction('SetSP', `{"spName":"currentEnterPriseID", "spValue":"${userData.EnterPriseID}"}`, '')
    VueCookies.set('currentEnterPriseID', userData.EnterPriseID)
    // 企業 app 點擊紀錄 +1
    commit('setAppClicks', userData.EnterPriseID)
    // 先存入 currentAppUser
    await commit('setCurrentAppUser', userData)
  },
  /* 企業公開資料相關 */

  // 取首頁所需資料
  async fetchData4CardIndex({dispatch}){
    // console.log('fetchData4CardIndex ===> start')

    dispatch('fetchIndexSlideBannersWuhu')
    // 自動領券(要在fetchVipAndCardInfoWuhu之前且要await)
    //console.log("xxxx>>首頁/card");
    //await dispatch('fetchGetDrawAutoTicketWuhu') //20201028暫時在首頁不自動領
    //dispatch('fetchVipAndCardInfoWuhu') // for 點數 + QR Code
    dispatch('fetchVipAndCardInfoWuhu', {type: ';7;8;'}) // for 點數 + QR Code    
  },


  // (公開) 取 currentApp 的公開資料 - HomeCoupon頁
  async fetchAppPublicDataCouponWuhu({dispatch}){
    // 自動領券(要在fetchMemberTicketListDataWuhu之前且要await)
    //console.log("xxxx>>1/cardCoupon我的優惠券");
    await dispatch('fetchGetDrawAutoTicketWuhu')

    // 取優惠卷列表資訊
    dispatch('fetchMemberTicketListDataWuhu')	// 抓-可使用的
    // 取會員已轉贈的優惠卷資訊
    dispatch('fetchMemberGaveTicketListDataWuhu')	// 抓-已轉贈的
    // 取尚未能使用的優惠卷資料
    dispatch('fetchMemberNotAvailableTicketsWuhu')	// 抓-未生效的
  },

  // (公開) 取 currentApp 的公開資料 - card/Member頁
  async fetchAppPublicDataDetailMemberWuhu({dispatch}){
    // 自動領券(要在fetchVipAndCardInfoWuhu之前且要await)
    //console.log("xxxx>>2會員專區");
    //await dispatch('fetchGetDrawAutoTicketWuhu') //20201028暫時在會員專區不自動領

    //dispatch('fetchVipAndCardInfoWuhu') // for 點數 + QR Code
    dispatch('fetchVipAndCardInfoWuhu', {type: ';7;8;'}) // for 點數 + QR Code
    // 取優惠卷列表資訊
    //dispatch('fetchMemberTicketListDataWuhu') //暫時拿掉,因為fetchVipAndCardInfoWuhu有支援TicketCount
  },

  // (公開) 取 currentApp 的公開資料 - card/Point
  async fetchAppPublicDataDetailPointWuhu({dispatch}){
    // 取點數等資料
    dispatch('fetchVipAndCardInfoWuhu')
    // 取會員的積點交易紀錄
    dispatch('fetchMemberExpiredPointsDataWuhu')
  },

  // (公開) 取 currentApp 的公開資料 - card/Prepay
  async fetchAppPublicDataDetailPrepayWuhu({dispatch}){
    dispatch('fetchVipAndCardInfoWuhu') // for 點數
    dispatch('fetchMemberDepositRecordDataWuhu') // for 儲值紀錄
  },

  // (公開) 取 currentApp 的公開資料
  async fetchAppPublicData({dispatch}){
		// console.log('JustNoWay: ', GetJsonTest);	// @@

    // 取企業 app 的 connect
    await dispatch('fetchAppConnect')
    // 取企業 app 的 LOGO
    dispatch('fetchAppLogo')
    // 取首頁主 banners
    dispatch('fetchIndexSlideBannersWuhu')
    // 取首頁副 banners
    dispatch('fetchIndexSubBannerWuhu')
    // 取最新消息的資料
    dispatch('fetchNewsData')
    // 取最新消息的分類
    dispatch('fetchNewsTypes')
    // 取品牌資訊的資料
    dispatch('fetchBrandInfo')
    // 取門店資訊的資料
    dispatch('fetchStoreListData')
    // 取 menu 資料
    dispatch('fetchMenuData')
  },
  async fetchAppBrandData({dispatch}, payload){
    await dispatch('fetchCompanyData')
    dispatch('fetchBrandLink')
    await dispatch('fetchBrandNews')
    dispatch('fetchCloudsearch')
    if (payload && payload.gid) dispatch('fetchBrandData', {GID: payload.gid})

  },
  async fetchAppBrandDataById({dispatch, state}, payload){
    // console.log('fetchAppBrandDataById ===> payload =>', payload)
    await dispatch('fetchCompanyData')

    let id = payload ? payload.gid : null
    let currentCompany = state.baseInfo.publicData.companyData.find((item)=>{
      return item.ID == id
    })
    let gid = currentCompany.GID
    dispatch('fetchBrandLink')
    await dispatch('fetchBrandNews')
    dispatch('fetchCloudsearch')
    gid && dispatch('fetchBrandData', {GID: gid})

  },
  async fetchBrandDataGID({dispatch}, payload){
    payload.gid && dispatch('fetchBrandData', {GID: payload.gid})
  },
  // (公開) 取企業 App 的 db connect 資料
  async fetchAppConnect({state, commit}){
    const url = state.api.memberUrl
    const body = {"act":"db_name","EnterPriseID": state.currentAppUser.EnterPriseID}
    const connect = await fetchData({url, body})
    if (connect.error) return

    commit('setAppConnect', connect)
  },
  // (公開) 取企業 App 的 LOGO 資料
  async fetchAppLogo({state, getters, commit}){
    const url = state.api.memberUrl
    const body = {act: "getapplogo", EnterPriseID: getters.currentApp.EnterpriseID}
    const data = await fetchData({url, body})
    if (data.error) return

    const logoUrl = getters.srcUrl + data[0].Dir + data[0].FileName

    commit('setAppLogo', logoUrl)
  },

  // (公開) 取企業 App 的 LOGO 資料
  async fetchAppLogoDetail({state, getters, commit}){
    const url = state.api.publicUrl
    const body = {act: "getapplogo", EnterPriseID: state.currentAppUser.EnterPriseID}
    const data = await fetchData({url, body})
    if (data.error) return

    const logoUrl = getters.srcUrl + data[0].Dir + data[0].FileName

    commit('setAppLogo', logoUrl)
  },

  // [五戶](公開) 取企業 App 的 LOGO 資料
  async fetchAppLogoWuhu({state, commit}){
    const url = state.api.memberUrl
    const body = {act: "getapplogo", EnterPriseID: state.baseInfo.EnterpriseID}
    const data = await fetchData({url, body})

    if (data.error) return

    const logoUrl = state.api.picUrl + data[0].Dir + data[0].FileName

    commit('setAppLogoWuhu', logoUrl)
  },

  // [五戶]撈[我的預約]清單
  async fetchMyBookList({getters, commit}){
    const url = getters.appBookUrlWuhu
    const body = getters.appBodyWuhu({act: 'get_mybooking'})
    const data = await fetchData({url, body})

    if (data.error) return

    commit('setMyBookList', data.mybooking)
    commit('setMyBookStatusList', data.status)
  },
  // [五戶]撈[商城]大小類等
  async fetchMallData({state}){
    const reloadShopFn = state.mallData.reloadShopFn;
    IsFn(reloadShopFn) && reloadShopFn();
  },
  // [五戶]撈[商城]記錄
  async fetchShopMine({state}){
    const reloadShopMineFn = state.mallData.reloadShopMineFn;
    IsFn(reloadShopMineFn) && reloadShopMineFn();
  },
  // [五戶]撈[商城]小類下品項,或品項的規格
  async fetchMallKindData({state},query){
    let isMallKindLoad = (query.goto!=undefined)//商城只允許第一個小類可刷新
    let isMallProductLoad = (query.type!=undefined && query.type === 'product');

    if (isMallProductLoad && IsFn(state.mallData.reloadProductFn)){
      state.mallData.reloadProductFn();

    }else if (isMallKindLoad && IsFn(state.mallData.reloadGoKindFn)) {
      state.mallData.reloadGoKindFn();
    }
  },
	/** @Remember: 廣告(AD)檔案做在get_advertgroup.json,並非GetBrandBanner */
  // (公開) 取廣告業資料
  async fetchAdvertise({commit}){
    const _act = 'get_advertgroup'
		let data = await GetJsonData1(_act)
		/** @@Test 彈跳廣告 */
		// data = await GetJsonTest(_act)	// @@
		data.ADBanner = FilterOnSale.call(data.ADBanner, 'No');
		// console.log('fetchAdvertise-data: ', data);	// @@

    commit('setAdvertise', data)
  },
  // (公開) 取公司資料: 隱私權政策,關於我們...
  async fetchCompanyData({commit}){
		let data = await GetJsonData1('get_cloudbrand');

		/** @@Test 副Banner資料 */
		// data = await GetJsonTest('get_cloudbrand')	// @@

		data = FilterOnSale.call(data, 'ID');
		commit('setCompanyData', data)
	},
	// (公開) 取公司資料: 雲商品
	async fetchBrandData({state, getters, commit}, payload){
    const url = state.api.figUrl
    const body = getters.appBodyWuhu({act:"get_brandproduct",GID:payload.GID})
    let data = await fetchData({url, body})

    if (data.error) return commit('setBrandData', [])
    commit('setBrandData', data)
  },
	// (公開) 取公司資料: 雲商品,取 KindID
	async fetchBrandDataKind({state, getters, commit}, payload){
    const url = state.api.figUrl
    const body = getters.appBodyWuhu({act:"get_brandproduct",KindID:payload.KindID})
    let data = await fetchData({url, body})
    if (data.error) return commit('setBrandDataKind', [])
    commit('setBrandDataKind', data)
  },
	// (公開) 取公司資料: 雲商品,取 DetailID
	async fetchBrandDataFood({state, getters, commit}, payload){
    const url = state.api.figUrl
    const body = getters.appBodyWuhu({act:"get_brandproduct",DetailID:payload.DetailID})
    let data = await fetchData({url, body})
    if (data.error) return commit('setBrandDataDetail', [])
    commit('setBrandDataDetail', data)
  },
  //品牌下的社群
  async fetchBrandLink({state, getters, commit}){
		const _act = 'get_brandlink'
		let data = await GetJsonData1(_act)
		if (!data.length) {	// 打api
			const url = state.api.figUrl
			const body = getters.appBodyWuhu({act:_act})
			data = await fetchData({url, body})
			if (data.error) return
		}
    commit('setBrandLink', data)
  },
  //品牌下的消息
  async fetchBrandNews({state, getters, commit}){
		const _act = 'get_brandmsg'
		let data = await GetJsonData1(_act)
		if (!data.length) {	// 打api
			const url = state.api.figUrl
			const body = getters.appBodyWuhu({act:_act})
			data = await fetchData({url, body})
			if (data.error) return
		}
		/** @@Test 最新消息 */
		// data = await GetJsonTest(_act)	// @@
		data = FilterOnSale.call(data, 'No');
		// console.log('fetchBrandNews-get_brandmsg: ', data);	// @@

    commit('setBrandNews', data)
  },
  //品牌下的某一筆消息
  async fetchBrandNewsById({state, getters, commit}, payload){
		const _act 	= 'get_brandmsg'
			,_id			= payload
		let data = await GetJsonData1(_act)
		if (!data.length) {	// 打api
			const url = state.api.figUrl
			const body = getters.appBodyWuhu({act:_act, "DetailID":_id})
			data = await fetchData({url, body})
			if (data.error) return
		}

		/** @@Test 最新消息 */
		// data = await GetJsonTest(_act)	// @@
		// console.log('fetchBrandNewsById-get_brandmsg: ', data);	// @@

		let arr1 = data.filter((item) => {return item.DetailID == _id});
		// console.log('GetJSON_BrandNews_id,arr1: '+_id, arr1); // @@
    commit('setBrandNewsOne', arr1)
  },
   //搜尋列設定
   async fetchCloudsearch({state, getters, commit}){
    const url = state.api.figUrl
    const body = getters.appBodyWuhu({act:"get_cloudsearch"})
    let data = await fetchData({url, body})
    if (data.error) return
    commit('setCloudsearch', data)
  },
  // (公開) 取主畫面可滑動的 Banners 資料
  async fetchIndexSlideBannersWuhu({commit}, payload){
		let data = await GetJSON_Banner(payload, 'M')
    if (data.error) return
    commit('setIndexSlideBannersWuhu', data)
  },

  // (公開) 取固定的副 Banner 資料
  async fetchIndexSubBannerWuhu({commit}, payload){
		let data = await GetJSON_Banner(payload, 'D')

    if (data.error) return
		/** @@Test 副Banner資料 */
		// let data = await GetJsonTest('subBanner')	// @@
    commit('setIndexSubBannerWuhu', data)
  },
  // (公開) 取最新消息的資料
  async fetchNewsData({state, commit, getters}){
    const url = state.api.publicUrl
    const body = getters.appBody({act:"GetBrendNews"})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setNewsData', data)
  },
  // (公開) 取最新消息的資料 - 五互
  async fetchNewsDataWuhu({state, commit, getters}){
    const url = state.api.publicUrl
    const body = getters.appBodyWuhu({act:"GetBrendNews"})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setNewsDataWuhu', data)
  },
  // (公開) 取最新消息的分類
  async fetchNewsTypes({state, commit, getters}){
    const url = state.api.publicUrl
    const body = getters.appBody({act:"GetBrendNewsType"})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setNewsTypes', data)
  },
  // (公開) 取品牌資訊的資料	// hank:應該是舊資料,待拿掉...
  // async fetchBrandInfo({state, commit, getters}){
    // const url = state.api.kakarUrl
    // const body = { act: "getbrandmsg", EnterpriseID: getters.currentApp.EnterpriseID, brandid: "1,2,3,4,5,6,7,8,9" }
    // const data = await fetchData({url, body})
    // if (data.error) return

    // commit('setBrandInfo', data)
  // },
  // (公開) 取門店資訊的資料
  async fetchStoreListData({state, getters, commit}){
    // 取得主企業號 ID & connect
    const memberUrl = `https://${baseUrl}/Public/AppVipOneEntry.ashx`
    const bodyA = { act:'GetVipMainEnterpriseID',EnterpriseID: state.currentAppUser.EnterPriseID }
    const dataA = await fetchData({url: memberUrl, body: bodyA})
    const MainEnterPriseID = dataA.EnterpriseID
    const MainConnect = dataA.connect

    // 取得門店資料
    const urlB = state.api.publicUrl
    const bodyB = getters.appBody({act:"GetStoreList"})
    const stores = await fetchData({url: urlB, body: bodyB})

    // 取得門店的線上點餐參數
    const bodyC = {"EnterPriseID": MainEnterPriseID, "act": "GetAppMode"}

    // 將線上點餐的參數(.Mode)存入到門店資料裡面
    const storeMode = await await fetchData({url: memberUrl, body: bodyC})
    stores.forEach(store => {
      const mode = storeMode.find(m => m.ShopID === store.OrgCode)
      store.Mode = (mode) ? JSON.parse(mode.Mode) : undefined
      store.ShopID = (mode) ? mode.ShopID : undefined
    })

    // 存入到 Vuex 裡面
    commit('setStoreListData', {stores, MainEnterPriseID, MainConnect})
  },

  // (公開) 取門店資訊的資料
  async fetchStoreListDataDetail({state, getters, commit}){
    // console.log('fetchStoreListDataDetail ===> start')
    // 取得主企業號 ID & connect
    const memberUrl = `https://${baseUrl}/Public/AppVipOneEntry.ashx`
    const bodyA = { act:'GetVipMainEnterpriseID',EnterpriseID: state.currentAppUser.EnterPriseID }
    const dataA = await fetchData({url: memberUrl, body: bodyA})
    const MainEnterPriseID = dataA.EnterpriseID
    const MainConnect = dataA.connect

    // 取得門店資料
    const urlB = state.api.publicUrl
    const bodyB = getters.appBody({act:"GetStoreList"})
    const stores = await fetchData({url: urlB, body: bodyB})

    // 取得門店的線上點餐參數
    const bodyC = {"EnterPriseID": MainEnterPriseID, "act": "GetAppMode"}

    // 將線上點餐的參數(.Mode)存入到門店資料裡面
    const storeMode = await await fetchData({url: memberUrl, body: bodyC})
    stores.forEach(store => {
      const mode = storeMode.find(m => m.ShopID === store.OrgCode)
      store.Mode = (mode) ? JSON.parse(mode.Mode) : undefined
      store.ShopID = (mode) ? mode.ShopID : undefined
    })

    // 存入到 Vuex 裡面
    commit('setStoreListData', {stores, MainEnterPriseID, MainConnect})
  },

  // (公開) 取門店資訊的資料 - 五互
  async fetchStoreListDataWuhu({state, getters, commit}){
		const _act1 = 'GetVipMainEnterpriseID'
			,_act2 		= 'GetStoreList2'
			,_act3 		= 'GetAishileAppMode'
			,EPID 		= state.baseInfo.EnterpriseID
			// ,memberUrl 	= `https://${baseUrl}/Public/AppVipOneEntry.ashx`
			,memberUrl	= 'https://8012.jh8.tw/Public/AppVipOneEntry.ashx'

		let dataA			= S_Obj.read(_act1)
			,stores			= S_Obj.read(_act2)
			,storeMode	= S_Obj.read(_act3)
			,MainEnterPriseID, MainConnect
		// console.log('S_Obj取得資料: ', dataA, stores, storeMode);	// @@

		// 取得主企業號 ID & connect
		if (!dataA) {
			const bodyA = {act:_act1, EnterpriseID: EPID}
			dataA = await fetchData({url: memberUrl, body: bodyA})
			S_Obj.save(_act1, dataA);
			// console.log('取得主企業號-GetVipMainEnterpriseID: ', dataA);	// @@
		}

		MainEnterPriseID = dataA.EnterpriseID
		MainConnect = dataA.connect

		// 取得門店資料
		if (!stores) {
			const urlB 	= state.api.publicUrl
				,bodyB	= getters.appBodyWuhu({act:_act2})
			stores = await fetchData({url: urlB, body: bodyB})
			S_Obj.save(_act2, stores);
			// console.log('取得門店資料-GetStoreList2: ', stores);	// @@
		}

		//取得門店的線上點餐參數
		if (!storeMode) {
			const bodyC = {act:_act3, EnterpriseID: EPID, FunctionID: "310703"}
			// 將線上點餐的參數(.Mode)存入到門店資料裡面
			storeMode = await fetchData({url: memberUrl, body: bodyC})
			storeMode.length && S_Obj.save(_act3, storeMode);
			// console.log('線上點餐的參數-storeMode: ', storeMode);	// @@
		}

    state.baseInfo.publicData.storeIsLoaded = true
    // 存入到 Vuex 裡面
    commit('setStoreListDataWuhu', {stores, MainEnterPriseID, MainConnect, storeMode})
  },

  // 取得門店頁需要的資料
  fetchStoreData({dispatch}){
    dispatch('fetchStoreListDataWuhu')
    dispatch('fetchCloudsearch')
  },

  // (公開) 取 Menu 資料
  async fetchMenuData({state, getters, dispatch}) {
    const url = state.api.publicUrl
    const body = getters.appBody({act:"GetBrandProductType"})
    const menuKindIds = await fetchData({url, body})
    if (menuKindIds.error) return
    menuKindIds.forEach(item => dispatch('fetchMenuMainTypeData', {code: item.KindID, name: ''}) )
  },
  // (公開) 取 Menu 大類的資料
  async fetchMenuMainTypeData({state, commit, getters}, payload) {
    // 如果已經有資料則離開
    const dataIsOwned = state.publicData.menu.mainType.find(item => { return item.code === payload.code }) !== undefined
    if (dataIsOwned) return

    const url = state.api.publicUrl
    const body = getters.appBody({act:"GetBrandProductType", MKindID: payload.code})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMenuMainTypeCodeName', payload)
    commit('setMenuMainTypeListData', { code: payload.code, data: data })
  },

  // (公開) 取某個雲商品的資料 (用DetailID)
  async fetchProductData({state, getters, commit}, payload){
    // console.log('fetchProductData ===> start')
    const url = state.api.figUrl
    const body = getters.appBodyWuhu({act:"get_brandproduct",DetailID:payload})
    let data = await fetchData({url, body})
    if (data.error) return commit('setCurrentProduct', [])
    commit('setCurrentProduct', data)
  },

  // (公開) 取 Menu 小類的資料
  async fetchMenuSubTypeData({state, commit, getters}, payload) {
    // 如果已經有資料則離開
    const dataIsOwned = state.publicData.menu.subType.find(item => item.code === payload.code ) !== undefined
    if (dataIsOwned) return

    const url = state.api.publicUrl
    const body = getters.appBody({act:"GetBrandProduct", KindID: payload.code})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMenuSubTypeCodeName', payload)
    commit('setMenuSubTypeListData', { code: payload.code, data: data } )
  },
  fetchFoodmarketMulti({state,dispatch}){
    // console.log("fetchFoodmarketMulti",state.mallData.foodkind);
    //迴圈一起跑會GG (API無法負荷??),已改為1筆1筆取,第一筆回應,再next
    if (Array.isArray(state.mallData.foodkind) && state.mallData.foodkind.length > 0) dispatch('fetchFoodmarketData',{kindID:state.mallData.foodkind[0]["ID"],kIndex:0})
  },
  // 取全部小類下商品
  async fetchFoodmarketData({commit,getters,dispatch},p_obj) {
    const url =  getters.appBookUrlWuhu;
    // console.log("fetchFoodmarketData",url,p_obj.kindID,p_obj.kIndex);
    const body = getters.appBodyWuhu({"act": 'get_foodmarket',"FoodKind": p_obj.kindID});
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0";

    if (isErr || data.error) return
    commit('setFoodmarketData', {foods: (data.FoodMarket || []), kindID: p_obj.kindID})
    const p_index = p_obj.kIndex+1;
    //已改為1筆1筆取,等待第一筆回應,再next
    if (state.mallData.foodkind.length > p_index) dispatch('fetchFoodmarketData',{kindID:state.mallData.foodkind[p_index]["ID"],kIndex:p_index})

  },
  /* 企業會員資料相關 */
  // 取會員的消費紀錄
  async fetchMemberPurchaseRecordData({commit, getters}) {
    const url = state.api.memberUrl
			,body 	= getters.appBodyWuhu({"act":"GetVipPosItems"})
			,data 	= await fetchData({url, body})
    if (data.error) return

		/** @@Test 購買交易紀錄 */
		// let data = await GetJsonTest('purchaseRecord')	// @@
    commit('setMemberPurchaseRecordData', data)
  },
  // 取會員的積點交易紀錄
  async fetchMemberExpiredPointsData({state, commit, getters}) {
    const url = state.api.memberUrl
    const body = getters.appBody({"act":"GetVipRecord", "TradeTypeCode":"4,-4,6,-6,7,-7,15,-15,16,-16,17,-17,71,-71,72,-72"})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberExpiredPointsData', data)
  },
  // 取會員的積點交易紀錄 - 五互
  async fetchMemberExpiredPointsDataWuhu({state, commit, getters}) {
    const url = state.api.memberUrl
    // 1      : 激活
    // 4,   -4: 積分兌換, 積分兌換退回
    // 6,   -6: 結帳消費, 結帳消費退回
    // 7,   -7: 積分換券, 積分換券退回
    // 13     : 卡片升級              (沒有-13)
    // 15, -15: 公關贈點, 公關贈點退回
    // 16, -16: 活動贈點, 活動贈點退回
    // 17, -17: 商品贈點, 商品贈點退回
    // 71, -71: 點數轉贈, 點數轉贈退回
    // 72, -72: 點數受贈, 點數受贈退回
    const body = getters.appBodyWuhu({"act":"GetVipRecord", "TradeTypeCode":"1,4,-4,6,-6,7,-7,13,15,-15,16,-16,17,-17,31,-31,71,-71,72,-72"})
    const data = await fetchData({url, body})
		// console.log('取會員的積點交易紀錄-data: ', data);	// @@
    if (data.error) return
    state.baseInfo.memberData.expiredPointsIsLoaded = true

    commit('setMemberExpiredPointsDataWuhu', data)
  },
  // (香繼光專屬) 取會員已登記的一代卡資料
  async fetchMemberRegisteredCardsData({state, commit, getters}){
    const url = state.api.jgssgUrl
    const body = getters.appBodyWuhu({"act":"iccardchklist"})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberRegisteredCardsData', data)
  },
  // 取會員的儲值交易紀錄
  async fetchMemberDepositRecordDataWuhu({state, commit, getters}) {
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({"act":"GetVipRecord", "TradeTypeCode":"3,-3,2,-2,19"})
    const data = await fetchData({url, body})
    if (data.error) return
    state.baseInfo.memberData.depositRecordIsLoaded = true

		/** @@Test 可領用的優惠卷列表 */
		// let data = await GetJsonTest('depositRecord')	// @@
    commit('setMemberDepositRecordData', data)
  },
  // 取會員的[消費點數紀錄]=[契約] - 五互
  // 秋林版 - 目前已改用黃大哥版(如下)
  async fetchMemberConsumerPointsDataWuhu_bak({state, commit, getters}) {
    const url = state.api.memberUrl
    const body = getters.appBodyWuhuEncode({
      "act":"WUHU_Deed_Points_Info_List_all",
      "FunctionID":"950201",
      "orderby":"end_day desc, deed_no desc",
      "srow": 0 // srow=0代表傳全部
    })
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberConsumerPointsDataWuhu', data)
  },
  // 取會員的[消費點數紀錄]=[契約] - 五互
  async fetchMemberConsumerPointsDataWuhu({/*state,*/ commit, getters}, payload) {
    const url = getters.appBookUrlWuhu

    const dynamicBody = (payload || {})
    let fixData = {
      "act":"get_deedpoints",
      "orderby":"end_day desc, deed_no desc"
    }
    let allData = Object.assign({}, fixData, dynamicBody)

    const body = getters.appBodyWuhu(allData)
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberConsumerPointsDataWuhu', data)
  },
  // 取會員的單一筆[契約][消費點數紀錄]=[契約] - 五互
  async fetchMemberConsumerPointsDetailWuhu({state, commit, getters}, payload) {
    const url = state.api.memberUrl
    const body = getters.appBodyWuhuEncode({
      "act":"WUHU_Deed_PointsBalance_Trade",
      "FunctionID":"950201",
      "orderby":"LastModify",
      "srow": 0 // srow=0代表傳全部
    }, {
      "deed_no": window.btoa(payload.deed_no)
    })
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberConsumerPointDetailWuhu', data)
  },
    //取得單一筆未完成訂單
  async fetchUnfinishorder_single({state,commit, getters}, payload) {
    commit('setSingleUnFinish', [])
    //const url = getters.subConsumerUrl
    const url = state.api.memberUrl
    const body = getters.appBodyWuhuEncode({
      "act":"get_unfinishorder_qry",
      "FunctionID":"950201",
      "Account": state.member.code,
      //"orderby":"LastModify",
      "srow": 0 // srow=0代表傳全部
    }, {
      "OrderID": window.btoa(payload.ID)
    })
    const data = await fetchData({url, body})

    if (data.error) return

    commit('setSingleUnFinish', data)
  },
   //取得已完成訂單
  async fetchOrder_single({state,commit, getters}, payload) {
    commit('setSingleFinish', [])
    //const url = getters.subConsumerUrl
    const url = state.api.memberUrl
    const body = getters.appBodyWuhuEncode({
      "act":"get_myorder_qry",
      "FunctionID":"950201",
      "Account": state.member.code,
      //"orderby":"LastModify",
      "srow": 0 // srow=0代表傳全部
    }, {
      "OrderID": window.btoa(payload.ID)
    })
    const data = await fetchData({url, body})

    if (data.error) return

    commit('setSingleFinish', data)
  },
    //取得預設通訊錄GID
  async fetchDefault_TbKey({state,commit, getters}) {
    if (!getters.isLogin)		return
    commit('setDefaultDelivery', {isLoad:false})
    //const url = getters.subConsumerUrl
    const url = state.api.memberUrl
    const body = getters.appBodyWuhuEncode({
      "act":"getTbDefu_950201",
      "FunctionID":"950201",
      "Account": state.member.code,
    }, {      
      "name": window.btoa('VIP_Delivery_Addr'),     
      "type": window.btoa('Delivery')
    })
    const data = await fetchData({url, body})
    commit('setDefaultDelivery', {isLoad:true})    
    if (data.error) return
    
    if (Array.isArray(data) && data.length){      
      data[0]['isLoad'] = true;
      commit('setDefaultDelivery', data[0])
    } 
    
  },
    //新增/更新存入預設通訊錄GID
  async fetchDefault_TbKey_update({state, commit, getters, dispatch}, payload) {   
    //const url = getters.subConsumerUrl
    //const {EnterpriseID} = state.baseInfo
    if (!getters.isLogin)		return
    const url = state.api.memberUrl
    if (!state.mallData.shipDataDefault.isLoad) return;//有撈過,key才能比對.
    const haveTb = state.mallData.shipDataDefault.TB_Name == 'VIP_Delivery_Addr' //存在時update
    let encodeBody = {  
      "name": window.btoa('VIP_Delivery_Addr'),
      "tbgid": window.btoa(payload.GID),
      "type": window.btoa('Delivery')
    }
    if (!haveTb){
      encodeBody.enterpriseID= window.btoa(state.baseInfo.EnterpriseID)
    }    
    const body = getters.appBodyWuhuEncode({
      "act":(haveTb?"setTbDefu_950201":"newTbDefu_950201"),
      "FunctionID":"950201",     
      "FunctionType":"1",
      "Account": state.member.code,
    },encodeBody )
    const data = await fetchData({url, body})
    if (data.error || data != 1) return
    if (state.mallData.shipDataDefault.TB_Name){
      let tmpDefault = JSON.parse(JSON.stringify(state.mallData.shipDataDefault))//防止記憶為舊的.
      tmpDefault.TB_GID = payload.GID
      commit('setDefaultDelivery', tmpDefault)
    }else{
      //新增(Default不存在)時重取
      await dispatch('fetchDefault_TbKey')
    }
    
  },
  //取得使用者選的超商Info
  async fetchOrder_cvsShop({state,commit, getters}, payload) {
    //localhost以及staging要在test資料庫撈
    commit('setCvsShopInfo', [])
    //let url = getters.subConsumerUrl;
    let url = state.api.memberUrl
    // if (S_Obj.isDebug || S_Obj.isStaging) url = "https://8012test.jh8.tw/public/AppDataVIP.ashx"
    const body = getters.appBodyWuhuEncode({
      "act":"myCvsShop_950201",
      "FunctionID":"950201",
      "Account": state.member.code,
      //"orderby":"LastModify",
      "srow": 0 // srow=0代表傳全部
    }, {
      "OrderID": window.btoa(payload.ID)
    })
    const data = await fetchData({url, body})

    if (data.error) return
    commit('setCvsShopInfo', data);
    if (IsFn(payload.fn)) payload.fn();
  },
  async calcuSurl({state,commit}, formData) {
    formData.qrType = formData.qrType || "2";
    formData.exType = formData.exType || "1";
    formData.data = formData.data || "{}";
    setLoading(true)
    const url = "https://surl.jh8.tw/AppShortURL.ashx"
		const {EnterpriseID} = state.baseInfo
		var body = { act: "SetUrl",
									EnterPriseID: EnterpriseID,
									BackData: formData.data,
									QrCodeType: formData.qrType,//1:url直接轉址,2:只運行 BackData
									ExpiredType: formData.exType //1:限1次,0:無限,2:限3天
								};
    if (formData.url) body.BackUrl = formData.url;
		const head = { "headers": { "Content-Type": "application/x-www-form-urlencoded" } }
		const {data} = await axios.post( url, body, head )
		commit('setLoading', false)
		if (data.ErrorCode != '0') showAlert(data.ErrorMsg || "")
    if (IsFn(formData.fn)) formData.fn((data.Remark || ''));
		return (data.Remark || '');
  },
  async goSurl({commit}, formData) {
    //限只會back data的jinher surl
		setLoading(true)    
		await axios.post( `https://surl.jh8.tw/${formData.sUrl}`)
		.then((res) => {
			commit('setLoading', false);
      IsFn(formData.fn) && formData.fn(res.data);
			
		}).catch( () => {
      IsFn(formData.fn) && formData.fn({});
			commit('setLoading', false)
		})

	},
  // 取會員的單一筆[訂單][消費點數紀錄] - 用Order No
  async fetchOneOrderByOrderNo({/*state,*/ commit, getters}, payload) {
    const url = getters.appBookUrlWuhu

    if (payload) {
      const dynamicBody = (payload.requestBody || {})
      let fixData = {
        "act":"get_orderitems"
      }
      let allData = Object.assign({}, fixData, dynamicBody)

      const body = getters.appBodyWuhu(allData)
      const data = await fetchData({url, body})
      if (data.error) return

      let orderDetail = data?data.OrderItem:null

      commit('setOneOrderDetail', {
        orderDetail: orderDetail,
        arrayIndex: payload.arrayIndex
      })
    }

  },
  async ensureGetDeviseGPS({/*state,*/ commit}) {
    //
    if (IsMobile()) {
      // (在殼裡面) call 跟殼取 GPS 資訊的 API 與 callback function
      deviseFunction('gps', '', '"cbFnSyncDeviseGPS"')
    } else {
      // 不在殼裡面
      if ("geolocation" in navigator) {
        /* geolocation is available */
        navigator.geolocation.getCurrentPosition((position)=>{
          //console.log('navigator ===> position =>', position)
          // 轉成 google maps api 需要的預設格式
          const gpsData = {gps: {
            lat: position.coords.latitude,
            lng: position.coords.longitude} }
          // 將 gps 存入 state.baseInfo
          commit('setBaseInfo', gpsData)
        }, ()=>{
          // console.log('navigator ===> error => no navigator')
        })
      } else {
        /* geolocation IS NOT available */
      }
    }
  },
  /* 取可領用的優惠卷列表資訊 + 領取優惠券 - 五互 */
  async fetchMemberDrawTicketListDataWuhu({state, commit, getters}){
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({ act: "GetDrawTicketList" })
    let data = await fetchData({url, body})
    if (data.error) return

		const isGetted = !!L_Obj.read('GetedDrawTicket_Time')
		isGetted && L_Obj.save('GetedDrawTicket_Json', data);

		// console.log('更新優惠卷列表data: ', data);	// @@

		/** @@Test 可領用的優惠卷列表 */
		// data = await GetJsonTest('GetDrawTicketList')	// @@
    commit('setMemberDrawTicketListData', data)
  },
  /* 取-可用的優惠卷列表資訊 - 快速時空門版 */
  async fetchGetDrawTicketListJson({state, commit, getters}){
  // async fetchGetDrawTicketListJson({commit}){
		// const _actTime	= 'GetedDrawTicket_Time'
			// // ,_act1				= 'GetDrawTicketListJson'
			// ,time1 				= L_Obj.read(_actTime)

		// if (time1) {
			// const now1	= formatTime(new Date(), 'YYYY-MM-DD HH:mm:ss')
			// ,nHours			= GetDateDiff(time1, now1, "h")
			// ,isOvered 	= nHours >= 1

			// if (isOvered) {
				// L_Obj.del(_actTime);
				// // L_Obj.del(_act1);
			// }
		// }

		// let data = L_Obj.read(_act1) || []
		// console.warn('readLS-data1: ', data);	// @@
		// if (!data.length) {
			const isWebProd 	= S_Obj.isWebProd
			const url = isWebProd ? state.api.memberUrl : state.api.Url8012
			const body = getters.appBodyWuhu({ act: "GetDrawTicketListJson" })
			let data = await fetchData({url, body})
			// console.warn('fetchGetDrawTicketListJson-data1: ', data);	// @@

			if (data.error) return
		// }

		/** @@Test 可用的優惠卷列表(快速時空門版 */
		// data = await GetJsonTest('DrawTicket')	// @@
		if (data.length) {
			const _format = 'YYYYMMDDHHmm'
				,today 			= formatTime(new Date(), _format)
			data = data.filter((item) => {
				const _s2 = formatTime(item.end_date, _format)
				return today <= _s2
			}).sort(function (a, b) {
				return parseInt(a["DrawNo"]) > parseInt(b["DrawNo"]) ? 1 : -1;
			})
		}

		// console.warn('過濾後_時空門優惠卷-data: ', data);	// @@
    commit('setMemberDrawTicketListData', data)
  },
  /* 自動領券(單純告訴後端,要開始領了的觸發act... */
  async fetchGetDrawAutoTicketWuhu({state, getters}){
    if (!getters.isLogin)		return
    const url = state.api.memberUrl
			,body 	=	getters.appBodyWuhu({ act: "getdrawautoticket" })
			,data 	= await fetchData({url, body})
    if (data.error) return

    return new Promise(resolve => {resolve(true)})
  },
  /* 取優惠卷列表資訊 */
  async fetchMemberTicketListData({state, commit, getters}){
    const url = state.api.memberUrl
			,body 	=	getters.appBody({ act: "GetVipTicket" })
			,data 	= await fetchData({url, body})
    if (data.error) return
    commit('setMemberTicketListData', data)
  },
  /* 取優惠卷列表資訊 - 五互,fig用此 */
  async fetchMemberTicketListDataWuhu({state, commit, getters}){
    if (!getters.isLogin)		return
    const url = state.api.memberUrl
			,body 	=	getters.appBodyWuhu({ act: "GetVipTicket" })
			,data 	= await fetchData({url, body})
    if (data.error) return

		/** @@Test 優惠卷列表資訊 */
		// let data = await GetJsonTest('ticketList')	// @@
    commit('setMemberTicketListDataWuhu', data)
  },
  // 取已贈送的優惠卷列表資訊
  async fetchMemberGaveTicketListData({state, commit, getters}){
    const url = state.api.memberUrl
			,body 	=	getters.appBody({ act: "GetGaveTicketRecord" })
			,data 	= await fetchData({url, body})
    if (data.error) return

    commit('setMemberGaveTicketListData', data)
  },
  // 取已贈送的優惠卷列表資訊 - 五互
  async fetchMemberGaveTicketListDataWuhu({state, commit, getters}){
    const url = state.api.memberUrl
			,body 	=	 getters.appBodyWuhu({ act: "GetGaveTicketRecord" })
			,data 	=  await fetchData({url, body})
    if (data.error) return

		/** @@Test 已贈送的優惠卷列表資訊 */
		// let data = await GetJsonTest('gaveTicketList')	// @@
    commit('setMemberGaveTicketListDataWuhu', data)
  },
  // 取尚未能使用的優惠卷資料
  async fetchMemberNotAvailableTickets({state, commit, getters}){
    const url = state.api.memberUrl
			,body 	=	getters.appBody({ act: "NotTodayTicketList" })
			,data 	= await fetchData({url, body})
    if (data.error) return

    commit('setMemberNotAvailableTickets', data)
  },
  // 取尚未能使用的優惠卷資料 - 五互
  async fetchMemberNotAvailableTicketsWuhu({state, commit, getters}){
    const url = state.api.memberUrl
			,body 	=	getters.appBodyWuhu({ act: "NotTodayTicketList" })
			,data 	= await fetchData({url, body})
    if (data.error) return

		/** @@Test 可領用的優惠卷列表 */
		// let data = await GetJsonTest('notAvailableTickets')	// @@
    commit('setMemberNotAvailableTicketsWuhu', data)
  },
  // 優惠卷贈送
  async ensureMemberTicketTrans({state, getters}, payload){
    const url = state.api.memberUrl
			,body 	=	getters.appBodyWuhu(Object.assign({}, { act: "SetTicketToOther" }, payload ))
			,data 	= await fetchData({url, body})
    if (data.error) return data.detail

    return data
  },
  // 點數贈送,轉贈-點數
  async ensureMemberTransPoint({state, getters}, payload){
    const url = state.api.memberUrl
			,body 	=	getters.appBodyWuhu(Object.assign({}, { act: "SetPointtoOther" }, payload ))
			,data 	= await fetchData({url, body})
    if (data.error) return data.detail

    return data
  },
  // 取優惠卷明細的使用條款與兌換條件
  fetchMemberTicketInfoData({dispatch}, ticket){
    // 取該卷使用的 QRcode
    dispatch('fetchTicketQRCodeData', ticket.GID)
    // 取該卷的條款與規則
    dispatch('fetchTicketTradeRules', ticket.TicketTypeCode)
  },
  // 取優惠卷條碼 QRcode
  async fetchTicketQRCodeData({state, commit, getters}, ticketGID){
    // 先清空現在的 QRcode 資料
    commit('setTicketQRCodeData', {gid: '', image: ''} )
    const url = state.api.memberUrl
			,body 	=	getters.appBodyWuhu({ act: "GetTicketQrCodeStr", GID: ticketGID, QRCodeScale: 4 })
			,data 	= await fetchData({url, body})
    if (data.error) return

    commit('setTicketQRCodeData', {gid: data.QrCode, image: data.QrCodeImg} )
  },
  // 取優惠卷明細資訊: 規則條款
  async fetchTicketTradeRules({state, commit, getters, dispatch}, TypeCode){
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({ act: "GenerateTicketTradeRules", TicketTypeCode: TypeCode })
    let data = await fetchData({url, body})
    if (data.error) return
    // console.log('取優惠卷明細資訊-data: ', data);	// @@

		/** @@Test 優惠卷明細資訊-規則條款 */
		// let data = await GetJsonTest('ruleData')	// @@

		const one = data[0]
    // 取優惠卷的兌換條件: 商品
    dispatch('fetchTicketUsageRuleFoods', one.TradeRuleCode)
    // 取優惠卷的兌換條件: 門店
    dispatch('fetchTicketUsageRuleShops', one.TradeRuleCode)
    // 設定使用條款與適用日
    commit('setMemberTicketInfoData', { ruleData: one, currentTypeCode: TypeCode })
  },
  // 取優惠卷明細資訊的兌換條件: 商品
  async fetchTicketUsageRuleFoods({state, commit, getters}, RuleCode){
    // 已有相同資料就不再取
    if (state.memberData.ticketInfo.currentRuleCode === RuleCode) return

    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({ act: "GenerateTicketTradeRuleFoods", TradeRuleCode: RuleCode })
    let data = await fetchData({url, body})
    if (data.error) return

		/** @@Test 優惠卷明細資訊的兌換-商品 */
		// let data = await GetJsonTest('usageFoods')	// @@
    commit('setMemberTicketInfoData', { usageFoods: data, currentRuleCode: RuleCode } )
  },

  // 取優惠卷明細資訊的兌換條件: 門店
  async fetchTicketUsageRuleShops({state, commit, getters}, RuleCode){
    // 已有相同資料就不再取
    if (state.memberData.ticketInfo.currentRuleCode === RuleCode) return

    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({ act: "GenerateTicketTradeRuleShops", TradeRuleCode: RuleCode })
    let data = await fetchData({url, body})
    // console.log('取優惠卷兌換門店-data: ', data);	// @@

    if (data.error) return

		/** @@Test 優惠卷明細資訊的兌換-門店 */
		// let data = await GetJsonTest('usageShops')	// @@
    commit('setMemberTicketInfoData', { usageShops: data, currentRuleCode: RuleCode })
  },
  // 取會員-推播消息-全部內容
  async fetchPushNotifyData({state, commit, getters}){
  // async fetchPushNotify({commit}){
    const url = state.api.memberUrl
			,isIOS	= state.baseInfo.AppOS === 'IOS' ? 1 : 0
			,body 	= getters.appBodyWuhu({act: "getpushmsg", PlatForm: isIOS})
			,data 	= await fetchData({url, body})
		let i = data.length;
		while (i--) {
			const o1 = data[i];
      o1.isOpen = false;
      o1.CreatDate = ReplaceDate(o1.CreatDate);
      o1.LastModify = ReplaceDate(o1.LastModify);
      o1.PushDate = ReplaceDate(o1.PushDate);
		}
		// console.log('推播消息全部內容-data: ', data);	// @@
    if (data.error) return

		/** @@Test 推播消息 */
		// let data = await GetJsonTest('pushNotify')
    commit('setPushNotifyData', data)
  },
  // 取會員-推播消息-總數
  async fetchPushNotifyNum({state, commit, getters}){
    const url = state.api.memberUrl
			,isIOS	= state.baseInfo.AppOS === 'IOS' ? 1 : 0
			,body 	= getters.appBodyWuhu({act: "GetPushMsgIsNotRead", PlatForm: isIOS})
			,data 	= await fetchData({url, body})
    if (data.error) return
    commit('setPushNotifyNum', data)
  },
  async fetchAppConfig({state, commit},str_keys) {
    const url = `https://${baseUrl}/public/GetAppSysConfigOneEntry.ashx?EnterpriseID=${state.baseInfo.EnterpriseID}&AppSysName=${str_keys}`
      ,body   ={}
      ,data 	= await fetchData({url, body})
    
    if (data && typeof data == 'string' && data.indexOf('OK,') == 0){
      const arr = data.split('OK,');
      if (arr.length > 1){
        const p_txt = `{${arr[1]}}`.replace(/'/g, "\"")
        const p_config = JSON.parse(p_txt);       
        //str_keys有可能為 foodMarketShopID,xxx,yyy...不能直接p_config[str_keys]
        commit('setMarketShopID', p_config.foodMarketShopID)
      } 
    }
  },
  async fetchMarketShopID({commit}) {
    const p_shopid = await getMarketShopID()   
    if (p_shopid) commit('setMarketShopID', p_shopid)    
  },
  
  // 設會員-推播消息-已讀
  updatePushNotify({state, getters}, GID){
    const url = state.api.memberUrl
			,body 	= getters.appBodyWuhu({act: "SetPushMsgIsRead", GID: GID})
			,data 	= fetchData({url, body})
		return new Promise(resolve => resolve(data))
  },
}


async function GetJSON_Banner(isBust, _type) {
	const arrObj = await GetJsonData1('GetBrandBanner')
	/** @@Test 主Banner資料 */
	// const arrObj = await GetJsonTest('GetBrandBanner')	// @@
	// console.log('GetJSON-_type: '+arrObj.length, _type); // @@

	switch(_type){
	case 'M':
		_type = "$M$"
		break;
	case 'D':
		_type = "$D$"
		break;
	default:	// AD
		_type = "AD"
	}

	let data = arrObj.length ? arrObj.filter((item) => {
		return item.BannerType == _type
	}) : []
	// console.log('GetJSON-arr1: ', data); // @@

	const isTrue = isBust || !data.length	// 有帶更新,或無值時...
	if (isTrue) {	// 打api
		const url = state.api.publicUrl
		const body = getters.appBodyWuhu({act:"GetBrandBanner", BannerType: _type})
		data = await fetchData({url, body})
	}
	// console.log('GetJSON_Banner-isBust: '+isBust, data);	// @@
	return FilterOnSale.call(data, 'No');
}

function FilterOnSale(_sort) {
	const today = formatTime(new Date(), 'YYYYMMDDHHmm')
	// console.log('FilterOnSale-today: '+_sort, today);	// @@

	if (this.length) {
		const them = this.filter((item) => {
			const _s1	= formatTime(item.SaleTime, 'YYYYMMDDHHmm')
				,_s2 		= formatTime(item.CloseTime, 'YYYYMMDDHHmm')
			// console.warn('Text,today: ', item.Text, today);	// @@
			// console.log('SaleTime, CloseTime: '+item.SaleTime, item.CloseTime);	// @@
			// const tt1 = item.SubText1 && item.SubText1.substr(0,16);	// @@
			// console.log('chNewsList-SubText1: ', tt1);	// @@
			return today >= _s1 && today <= _s2
		}).sort(function (a, b) {
			return parseInt(a[_sort]) > parseInt(b[_sort]) ? 1 : -1;
		})
		// !them.length && console.log('FilterOnSale-this,them: ', this, them);	// @@
		return them;

	} else {
		return [];
	}
}

export default new Vuex.Store({ state, mutations, getters, actions })
