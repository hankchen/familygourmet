/** 用Session Storage,暫存本機的東西 */
import axios from 'axios'
import store from 'src/store'
// import fetchData from 'src/libs/fetchData.js'

const EnterpriseID 	= '1108608537'
	,baseUrl 		= 'wuhu.jh8.tw'
	,publicUrl		= `https://${baseUrl}/public/newsocket.ashx`

/* [SessionStorage] 緩存(HTML5的內建obj) */
let S_Obj = {
	mac:	 '',
	isWebProd:	 /web/.test(location.host),
	isStaging:	 /staging/.test(location.host),	// !isWebProd
	isDebug:	 /localhost/.test(location.host) || /^192.168/ig.test(location.host),
	isFromTest: function () {
		const v2 = store.state.userData.RtAPIUrl;
		v2 && v2.trim().toLowerCase();
		// console.log('isFromTest-RtAPIUrl: ', v2); // @@
		return /8012test/.test(v2);
	},
	isTestAPI(){
		return this.isStaging && this.isFromTest();
	},
	save(key, v1){
		sessionStorage[key] = (typeof(v1) === 'object') ? JSON.stringify(v1) : v1;
	},
	read(key){
		const value = sessionStorage[key]
		return /{|]/.test(value) ? JSON.parse(value) : value;
		// return (typeof(value) === 'string') ? JSON.parse(value) : value;
	},
	// del(key){
		// sessionStorage.removeItem(key);
	// },
	// clear(){
		// sessionStorage.clear(); //刪除所有資料
	// },

};

/* [LocalStorage] 緩存(HTML5的內建obj) */
let L_Obj = {
	use: 'localStorage' in window && window.localStorage !== null,
    save(key, v1){
		localStorage[key] = (typeof(v1) === 'object') ? JSON.stringify(v1) : v1;
	},
	read(key){
		const value = localStorage[key]
		return /{|]/.test(value) ? JSON.parse(value) : value;
		// return (typeof(value) === 'string') ? JSON.parse(value) : value;
	},
	saveBa64(key, v1){		
		localStorage[key] = window.btoa(encodeURIComponent((typeof(v1) === 'object') ? JSON.stringify(v1) : v1));
	},
	readBa64(key){
		if (!localStorage[key]) return
		const value = decodeURIComponent(window.atob(localStorage[key]))
		return /{|]/.test(value) ? JSON.parse(value) : value;
		// return (typeof(value) === 'string') ? JSON.parse(value) : value;
	},
	del(key){
		localStorage.removeItem(key);
	},
	clear(){
		localStorage.clear();
	},
};


async function Init() {
	const body = {
		EnterpriseID,
		act: "GetMac",
	}
	const head = { "headers": { "Content-Type": "application/x-www-form-urlencoded" } }
	let {data} = await axios.post(publicUrl, body, head)
	// => 存入mac
	if (data.Remark) {
		const res1 = JSON.parse(data.Remark)
		S_Obj.mac = res1.mac
		// console.log('存入mac-mac: ', S_Obj.mac); // @@
	}

	// const _txt = 'Init-uData: IsMobile: '+typeof(JSInterface)
				// +', LocalStorage 支援: '+L_Obj.use
	// alert(_txt);	// @@
}

setTimeout(function(){ Init(); }, 100);

export { S_Obj, L_Obj }
