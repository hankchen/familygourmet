// vue basic lib
import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

/* 同路由不跳紅字 */
const routerPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(error=> error)
}

// vue router 設定(桌面版)
const routes = [
  // === 卡卡主頁面 ===
  { path: '/',      component: () => import('src/appFami/pages/Index.vue') },
  { path: '/login', component: () => import('src/appFami/pages/Login.vue') },
  { path: '/setting', component: () => import('src/appFami/pages/Setting.vue') },
  { path: '/register', component: () => import('src/appFami/pages/Register.vue') },
  { path: '/404' },
  // === 企業 APP 系列 ===
  // 企業首頁
  { path: '/card', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/Index.vue') },
  // 會員頁
  { path: '/cardMember', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/Member.vue') },
  // 積點紀錄
  { path: '/cardPoint', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/Point.vue') },
  // 點數贈點
  { path: '/pointGive', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/PointGive.vue') },
  // 消費紀錄
  { path: '/cardPay', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/Pay.vue') },
  // 儲值紀錄
  { path: '/cardPrepay', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/Prepay.vue') },
  // 優惠卷
  { path: '/cardCoupon', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/Coupon.vue') },
  // 會員資訊
  { path: '/cardProfile', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/Profile.vue') },
  // // 門市查詢
  { path: '/cardStore', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/Store.vue') },
  // 產品列表頁
  { path: '/cardMenu/:id', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/Menu.vue') },
  // // 產品介紹頁
  { path: '/cardProducts/:id', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/Products.vue') },
  // 消息列表頁
  { path: '/cardNews', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/News.vue') },
  // 消息介紹頁
  { path: '/cardNewspage/:id', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/Newspage.vue') },
  // 品牌專區
  { path: '/cardBrand', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/Brand.vue') },
  // 常見問題頁
  { path: '/cardFaq', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/Faq.vue') },
  // 商品禮卷(寄杯)頁
  { path: '/cardGiftVouchers', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/GiftVouchers.vue') },
  // 集點換券頁
  { path: '/pointExg', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/Exchange.vue') },
  // 限時搶券
  { path: '/getCoupon', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/GetCoupon.vue') },
  // 享聚卡回娘家
  { path: '/oldCard', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/OldCard.vue') },
  // 點數轉贈
  { path: '/pointGive', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/PointGive.vue') },
  // 推播訊息
  { path: '/pushNotifications', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/PushNotifications.vue') },

  // 商城首頁
  { path: '/cardShop/', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/Shop.vue') },
  // 購物車
  { path: '/cardShopCart/', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/ShopCart.vue') },
  // 訂單記錄
  { path: '/cardShopMine/', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/ShopMine.vue') },
  // 商品頁
  { path: '/cardShopType/', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/ShopType.vue') },
  // 物流詳情
  { path: '/cardShopTrace/:id', meta: { layout: 'Card' }, component: () => import('src/appFami/pages/cards/ShopTrace.vue') },
]

export default new VueRouter({ routes })
