/* 共用的 function */

// import axios from 'axios'
import store from 'src/store'
import { S_Obj, L_Obj } from 'src/s_obj.js'
import {GetJsonData1, FetchDrawMinute} from 'src/json1.js'	// 取得json數據

/* 亂數取一值 (最小值~最大值) */
function GetRandNum(min, max) {
  return Math.floor(Math.random()*(max-min+1)+min);
}
/** @Sample:
	var a = GetRandNum(1,20);		// 1~20 取一數
*/

/* 針對後端來源的日期字申多了個T */
function ReplaceDate(v1) {
	return v1 ? v1.replace(/T/g, ' ') : '';
}

/* 取得時間差 */
function GetDateDiff(startTime, endTime, diffType) {
	//將xxxx-xx-xx的時間格式，轉換為 xxxx/xx/xx的格式
	startTime = startTime.replace(/-/g, "/");
	endTime = endTime.replace(/-/g, "/");

	var sTime 	= new Date(startTime) 	// 開始時間
		,eTime 	= new Date(endTime)		// 結束時間
		,divNum = 1						// 作為除數的數字
	switch (diffType) {
		case "s":
			divNum = 1000;
			break;
		case "m":
			divNum = 1000 * 60;
			break;
		case "h":
			divNum = 1000 * 3600;
			break;
		case "d":
			divNum = 1000 * 3600 * 24;
			break;
		// default:
			// break;
	}
	return parseInt((eTime.getTime() - sTime.getTime()) / parseInt(divNum));
}

/** @Sample:
	var Diffday = GetDateDiff('2018/08/01', '2018/08/31', "d");
*/

/* 按縣市選門店 */
function StoreGroup() {
	let list = store.state.baseInfo.publicData.storeList.store || []
		,group_arr 	= GroupBy(list, "City")
		,o_group 	= {};
	group_arr.map(arr => {
		let city = arr[0]["City"];
		o_group[city] = arr;
	});
	return o_group;
}

/* 做排序 */
function GroupBy(collection, property) {
	var val, index,
	values = [], result = [];

	collection = collection || [];
	const nMax = collection.length
	for (var i = 0; i < nMax; i++) {
		val = collection[i][property];
		index = values.indexOf(val);
		if (index > -1) {
			result[index].push(collection[i]);
		} else {
			values.push(val);
			result.push([collection[i]]);
		}
	}
	return result;
}

/* 前端建GUID */
function GUID() {
  var d = Date.now();
  if (typeof performance == 'object' && typeof performance.now == 'function') {
    d += performance.now(); //use high-precision timer if available
  }
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
      return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
}

function IsFn(fn) {return typeof fn ==='function'}
// function IsLogin() {return store.getters.isLogin}

/* 取參數-Session Storage */
function GetURLPara(v1) {
	// const para1 = sessionStorage['URLPara']
	const para1 = S_Obj.read('URLPara') || ''
		,_find = para1.slice(para1.indexOf('?') + 1).trim()
	// console.warn('取參數_URLParameter: ', _find);	// @@
	if (_find) {
		let i, val, params = _find.split("&")
			,nlen = params.length
		for (i = 0; i < nlen; i++) {
			val = params[i].split("=");
			if (val[0] == v1) {
				return unescape(val[1]);
			}
		}
	}
	return null;
}

function SetURLPara(v1) {
	S_Obj.save('URLPara', v1);
}

/* 記錄上一頁 */
function SetLastPage(v1) {
	// console.warn('SetLastPage-v1: ', v1);	// @@
	v1 != '/' && S_Obj.save('LastPage', v1);
}

/* 取不到自動再抓json數據 */
async function ReadJsonS(key) {
	let data1 = S_Obj.read(key) || []
	// console.log('readSS-data1: ', data1);	// @@
	if (!data1.length) {
		data1 = await GetJsonData1(key) || []
		// console.log('readjson-data1: ', data1);	// @@
		data1.length && S_Obj.save(key, data1);
	}
	return data1;
}

/* 抓-微管雲參數 */
async function FetchSysConfig(key1) {
	let data1 = S_Obj.read(key1)
	if (!data1) {
		data1 = await FetchDrawMinute()
		//console.log('微管雲參數-data1: ', data1);	// @@
		S_Obj.save(key1, data1);
	}
	return data1;
}
/** @Sample:
	const set1 = FetchSysConfig('AppDrawMinute');
*/

const IsMobile = function() {
	return BMobile || typeof(JSInterface) !== 'undefined';
}

let BMobile = IsMobile()


export {
// StoreGroup, GroupBy, GUID, IsFn, IsLogin, IsMobile,
GetRandNum, ReplaceDate, GetDateDiff, StoreGroup, GroupBy, GUID, IsFn, IsMobile,
S_Obj, L_Obj, ReadJsonS, FetchSysConfig,
GetURLPara, SetURLPara, SetLastPage
}
