/* 系統環境參數相關 */

// 依據 環境參數(測試站) or 網址(正式站) 判定要取用的 app 版本
// const getAppSite = () => {
//   // 如果是測試環境，依據 .env.local 來判定
//   const isDev = /localhost/.test(location.href)
//   if (isDev) return process.env.VUE_APP_SITE

//   // 如果是非測試站，依據網址判定
//   const isKakar = /kakar/.test(location.href)
//   if (isKakar) return 'kakar'
//   const isWuhulife = /familygourmet/.test(location.href)
//   if (isWuhulife) return 'wuhulife'

//   // 預設： kakar
//   return 'wuhulife'
// }

// const appSite = getAppSite()

// // main App (kakar)
// import AppFami from 'src/AppFami.vue'
// import routerKakar from 'src/AppFami/router'

// // main App (wuhulife)
// import AppWuhulife from 'src/AppWuhulife.vue'
// import routerWuhulife from 'src/appWuhulife/router'

// // 依據 appSite 設定要顯示的 app & router
// let App, router
// switch(appSite) {
//   case 'kakar':
//     App = AppFami
//     router = routerKakar
//   break;
//   case 'wuhulife':
//     App = AppWuhulife
//     router = routerWuhulife
//   break;
// }

import AppFami from 'src/AppFami.vue'
import routerFami from 'src/appFami/router'

let App = AppFami
let router = routerFami

export { App, router }
