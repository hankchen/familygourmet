/* app 通用 helper  */

import Vue from 'vue'
import VueCompositionApi from '@vue/composition-api'
Vue.use(VueCompositionApi)

import store from '../store'
import { computed } from '@vue/composition-api'

// toast
import { Toaster } from "@blueprintjs/core"
const AppToaster = Toaster.create({ position: 'top', maxToasts: 2 })

// 觸發 Toast (訊息通知元件)
function showToast(message, config = {}) {
  const baseConfig = {intent: 'primary', icon: 'notifications', timeout: 2500 }
  const data = Object.assign({}, baseConfig, config, {message: message})
  return AppToaster.show(data)
  /**
    # 用法：

    * 一般
    => app.showToast('hello world')

    * 加入客制色系 & icon 的用法 (預設: {intent: 'primary', icon: 'notifications' } )
    => app.showToast('hello world', {intent: 'success', icon: 'issue' })

    * 可用參數：
    intent : primary, success, danger, warning
    icon   : 參考 https://blueprintjs.com/docs/#icons
  **/
}

function setLoading(status, mType) {store.commit('setLoading', {status: status, mType: mType})}

// 顯示自訂的 alert 畫面
function showAlert(text) { store.commit('setCustomModal', { type: 'alert', text }) }
function hideModal() { store.commit('setCustomModal', { type: '', text: '' }) }

// 顯示自訂的 confirm 畫面
// text: 大標題
// confirmText: 確認按鈕label
// cancelText: 取消按鈕label
// confirmNoteTitle: 附註標題
// confirmNoteChoice: 附註選項
// confirmNoteChoosed: 附註選項已選
// confirmNoteRemark: 附註選了其他要手key的說明
// confirmNoteRemarkPlaceHolder: 其他原因的place holder
// confirmNoteWarnMsg: 警告訊息
function showConfirm(text, confirmText, cancelText,
  confirmNoteTitle, confirmNoteChoice, confirmNoteChoosed, confirmNoteRemark,
  confirmNoteRemarkPlaceHolder, confirmNoteWarnMsg,confirmNoteText) {
  return new Promise((resolve, reject) => {
    store.commit("setCustomModal", {
      type: 'confirm',
      text, confirmText, cancelText, 
      confirmNoteTitle, confirmNoteChoice, confirmNoteChoosed, confirmNoteRemark,
      confirmNoteRemarkPlaceHolder, confirmNoteWarnMsg,confirmNoteText,
      resolve, reject
    });
  });
}

// 依據 cssTouch 參數設置 mobile 滑動的 css 參數
const touchScrolling = computed(() => {
  const cssTouch = store.state.cssTouch
  return {'-webkit-overflow-scrolling': (cssTouch) ? 'touch' : 'auto' }
})
function checkReceiver(vip) {
  var errMsg = "";
  const twMobilePhoneRegxp = /^09[0-9]{2}[0-9]{6}$/; // 台灣手機號碼格式需為09XXXXXXXX
  const cnMobilePhoneRegxp = /^1[0-9]{2}[0-9]{8}$/;  // 中國手機號碼格式需為1XXXXXXXXXX
  if (twMobilePhoneRegxp.test(vip.mobileTmp) === false && cnMobilePhoneRegxp.test(vip.mobileTmp) === false) errMsg = '手機號碼格式錯誤'
  if (vip.addrsTmp == "" || vip.receiverTmp == "") errMsg = (errMsg != ""? errMsg + '<br>收件人 或 地址未填寫':'收件人 或 收件人地址未填')
  return errMsg; 
}
function makeid_num(num) {
  num = num || 1;
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		for (var i = 0; i < num; i++)
			text += possible.charAt(Math.floor(Math.random() * possible.length));

		return text;
}
function removeEmojis(string_a) {
  if (!string_a) return "";
  var regex = /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|[\ud83c[\ude01\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|[\ud83c[\ude32\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|[\ud83c[\ude50\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g;
  
  var reg = /[~#^$@%&!?%*]/gi;
  return string_a.replace(regex, '').replace(reg, '');
}
function getParameterByName(name) {
  var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
  return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}
function b64_to_utf8(str_){
  if (typeof str_ === "string") return decodeURIComponent(atob(str_));
	return "";
}
function jParse(s){
  let pData ;
  try { 
    if (typeof s === "string"){
      pData = JSON.parse(s)
    }
    pData = pData || {}; 
  }
  catch(e) { pData = {} }
  return pData
}
// 是否為一般登入驗證前/後
function isNotLinePara(){
  const p_code = store.state.searchURL.code || "";
  const p_state = store.state.searchURL.state || ""; 
  const p_lineP = store.state.searchURL.linePara || ""
  const p_lineNtf = store.state.searchURL.lineNofInfo || ""  //LINE notify
  const t_code = getParameterByName("code") || "";
  const t_state = getParameterByName("state") || "";
  const t_lineP = getParameterByName("linePara") || "";  
  const t_lineNtf = getParameterByName("lineNofInfo") || "";//LINE notify
  return (!p_code && !p_state && !p_lineP && !p_lineNtf && !t_code && !t_state && !t_lineP && !t_lineNtf)
}
// 顯示圖檔(依據塞入的值，來決定回傳的值)
function showImgSubsidiary(imageUrl) {
  const pic = showImageWuhu(imageUrl);
  return pic.indexOf('/assets/images/img_init.png') > -1 ?require('../assets/images/img_init.png'):pic;
}
// 顯示圖檔(依據塞入的值，來決定回傳的值)
function showImage(imageUrl) {
  let isImage = false
  if (imageUrl) {
    // 是否為圖檔 (尾端必須為 .jpg, .jpeg, .png, .gif, .bmp)
    isImage = /.jpg|.jpeg|.png|.gif|.bmp/.test(imageUrl.toLowerCase())
  }
  // 若不是圖檔，回傳 noImageUrl
  if (isImage === false) return require('@/assets/images/img_init.png')

  // 是否為完整網址
  const isFullUrl = /http:|https:/.test(imageUrl)
  // 如果是，直接回傳 imageUrl
  if (isFullUrl) return imageUrl
  // 若不是，則回傳的值要補上 srcUrl
  return store.getters.srcUrl + imageUrl
}

// 顯示圖檔(依據塞入的值，來決定回傳的值)
function showImageWuhu(imageUrl) {
  let isImage = false
  if (imageUrl) {
    // 是否為圖檔 (尾端必須為 .jpg, .jpeg, .png, .gif, .bmp)
    isImage = /.jpg|.jpeg|.png|.gif|.bmp/.test(imageUrl.toLowerCase())  
  }
  
  // 若不是圖檔，回傳 notImage 字串
  if (isImage === false) return require('@/assets/images/img_init.png')

  // 是否為完整網址
  const isFullUrl = /http:|https:/.test(imageUrl)
  // 如果是，直接回傳 imageUrl
  if (isFullUrl) return imageUrl
  // 若不是，則回傳的值要補上 srcUrl
  return store.state.api.picUrl + imageUrl
}

// 判斷是否已登入
const isLogin = computed(() => store.getters.isLogin )

// 修正ios鍵盤擠壓頁面無法復原
function fixScroll() {
  let u = navigator.userAgent;
  let isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
  if (isiOS) {
    window.scrollTo({
      top: window.pageYOffset,
      behavior: 'smooth',
    });
  }
}
// closable 點擊外範圍收合
let handleOutsideClick
export const Closable = {
  bind (el, binding, vnode) {
    handleOutsideClick = (e) => {
      e.stopPropagation()
      const { handler, exclude } = binding.value
      let clickedOnExcludedEl = false
      exclude.forEach(refName => {
        if (!clickedOnExcludedEl) {
          const excludedEl = vnode.context.$refs[refName]          
          if (excludedEl) clickedOnExcludedEl = excludedEl.contains(e.target)
        }
      })
      if (!el.contains(e.target) && !clickedOnExcludedEl) {
        vnode.context[handler]()
      }
    }
    document.addEventListener('click', handleOutsideClick)
    document.addEventListener('touchstart', handleOutsideClick)
  },

  unbind () {
    document.removeEventListener('click', handleOutsideClick)
    document.removeEventListener('touchstart', handleOutsideClick)
  }
}
Vue.directive('closable', Closable)


export { showToast, showAlert, showConfirm, setLoading, hideModal, touchScrolling, isLogin, showImage, showImageWuhu, fixScroll, checkReceiver, isNotLinePara, showImgSubsidiary, jParse, b64_to_utf8, makeid_num, getParameterByName, removeEmojis }
