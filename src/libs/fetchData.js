import Vue from 'vue'
import VueCookies from 'vue-cookies'

import axios from 'axios'
import store from '../store'
import { S_Obj } from '../s_obj.js'

import { deviseFunction } from 'src/libs/deviseHelper.js'
import { showAlert, setLoading } from 'src/libs/appHelper.js'

Vue.use(VueCookies)

const baseUrl = 'https://wuhuapp.jh8.tw/Public/AppCloudOneEntry.ashx'
const baseHead = { "headers": { "Content-Type": "application/x-www-form-urlencoded" } }

// 將取得的資料做 JSON.parse, 並檢驗資料格式是否正確
function parse(data) {
  let pData ;
  if (typeof data.Remark === "string") {
    data.Remark = data.Remark.replace(/\t/g, '');
    // eslint-disable-next-line no-control-regex
    data.Remark = data.Remark.replace(//g, '');
    data.Remark = data.Remark.replace(/ {2}/g, "");
    // eslint-disable-next-line no-control-regex
    data.Remark = data.Remark.replace(//g,"");
    data.Remark = data.Remark.replace(/\v/g, "");
  }
  try { pData = JSON.parse(data.Remark) }
  catch(e) {
    //console.log('json error',e);
    pData = { error: true, msg: 'JSON parse failure', detail: e }
  }
  return pData
}

async function getToken(reqHead) {
  let tokenUrl = store.getters.appTokenUrlWuhu

  let tokenBody = {
    "act":"login",
    "memberphone":store.state.member.code,
    "memberPwd":store.state.member.pwd,
    "isFrom": store.state.baseInfo.isFrom,
    "EnterpriseID": store.state.baseInfo.EnterpriseID
  }
  let tokenData = await axios.post(tokenUrl, tokenBody, reqHead)

  let tokenDataObj = parse(tokenData.data)

  let newTokenkey = ''

  if (tokenDataObj && typeof tokenDataObj !== 'undefined'
    && tokenDataObj.length>0) {
    newTokenkey = tokenDataObj[0].Tokenkey //??? 當cookie被清空時 Uncaught (in promise) TypeError: Cannot read property '0' of null

    // console.log('fetchData.js getToken ===> new Tokenkey =>' + newTokenkey)

    // 更新密碼到cookie和store
    store.state.member.Tokenkey = newTokenkey
    // 非殼才要存到cookie
    if (typeof(JSInterface) == 'undefined') {
      // console.log('fetchData.js getToken ===> in browser => save token to cookies')
      VueCookies.set('kkTokenkey', newTokenkey)
    }

    // 更新token到殼
    let setThing = `{"spName":"Tokenkey", "spValue": "${newTokenkey}"}`
    // console.log('fetchData.js getToken ===> setThing =>' + setThing)
    deviseFunction('SetSP', setThing, '')
  }
  return newTokenkey
}

async function fetchData(payload={}) {
  const {url, body, head ,resIsString} = payload
		,reqBody 		= body || {}
		,reqHead 		= head || baseHead
		,isWebProd 	= S_Obj.isWebProd

  let reqUrl 		= url || baseUrl
		,RtAPIUrl 	= store.state.userData.RtAPIUrl
  if (RtAPIUrl && !isWebProd) {
		const getBaseUrl = store.getters.getBaseUrl
		RtAPIUrl = RtAPIUrl.replace('https://','')
		// console.log('getBaseUrl取代成-RtAPIUrl: '+getBaseUrl, RtAPIUrl);	// @@

    /** @PS: 依 store的baseUrl去取代掉
    fig.jh8.tw ==> 8012test.jh8.tw */
    reqUrl = reqUrl.replace(getBaseUrl, RtAPIUrl);
  }

  // 打 API 取資料
  let {data} = await axios.post(reqUrl, reqBody, reqHead)
		.catch ((err) => {
			console.error('API錯誤: ', err);
			setLoading(false);
		});

  // 999: token失效 => 導到登入頁
  if (data.ErrorCode === '999') {
    // console.log('token失效 ===> 登出到登入頁')
    showAlert('token失效,請重新登入')
    store.dispatch('memberLogout')
    data.ErrorCode = 0
    data.ErrorMsg = null

    setLoading(false)

  } else if (data.ErrorCode === '998') { // 998: token過期
		const isLogin = store.getters.isLogin
		// console.log('token過期 ===> isLogin =>' + isLogin)

    if (isLogin === false) {
      // console.log('token過期 ===> logout狀態不重撈token')
    } else {
      // console.log('token過期 ===> login狀態需重撈token')
      // 重撈token
      reqBody.Tokenkey = await getToken(reqHead)

      // 用重撈的token再call一次api
      data = await axios.post(reqUrl, reqBody, reqHead)
      if (data.data.ErrorCode === '0' && data.data.Remark !== '') {
        data = data.data
      }
    }
  }

  // 資料驗證
  if (data.error) return data  
  // 非秋林 API 的結構直接 return
  if (!data.ErrorCode && !data.ErrorMsg && !data.Remark) return data
  //不用parse
  if (resIsString) return (data.Remark || ''); 
  // 秋林 API 如果 Remark 是空字串 return
  if (data.ErrorCode === '0' && data.Remark === '') return data
  // 秋林 API 如果 Remark 是null return
  if (data.ErrorCode === '0' && !data.Remark) {data.Remark=''; return data}
  // 秋林 API 如果 ErrorMsg 有值 return
  if (data.ErrorCode === '0' && data.ErrorMsg !== '') {    
    const p_data = parse(data);
    p_data.ErrorMsg = data.ErrorMsg;
    return p_data;
  }
  // 秋林 API 如果報錯 return error
  if (data.ErrorCode !== '0') return { error: true, msg: data.ErrorMsg, detail: data }

  // 資料回傳  
  return parse(data)

}

export default fetchData
