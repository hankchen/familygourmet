import moment from 'moment'
// window.moment = moment
// 取得當下時間
const currentTime = () => {
  return moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
}
// 當下年份
const currentYear  = new Date().getFullYear()
// 當下月份
const currentMonth = new Date().getMonth() + 1

// 預設日期: 當天 (格式： '2019-09-19')
const today = moment(new Date()).format('YYYY-MM-DD')

// 顯示與目前時間的差距 (約 xx 時 oo 分 以前)
const agoTime = (time) => {
  const currentTime = moment(new Date())
  const createTime = moment(new Date(time))
  const diffSeconds = currentTime.diff(createTime, 'seconds') % 60
  const diffMinutes = currentTime.diff(createTime, 'minutes') % 60
  const diffHours = currentTime.diff(createTime, 'hours') % 24
  const diffDays = currentTime.diff(createTime, 'days')

  let string = ""
  if (diffDays    > 0) string += `${diffDays} 天 `
  if (diffHours   > 0) string += `${diffHours} 小時 `
  if (diffMinutes > 0) string += `${diffMinutes} 分 `
  string += `${diffSeconds} 秒前 `
  return string
}

// 將時間格式化
// 格式請參考
// https://momentjs.com/docs/#/parsing/string-format/
const formatTime = (timeStr, formatStr) => {
  // moment(this.singleDate).format('YYYY-MM-DD')
  return moment(timeStr).format(formatStr)
}

export { moment, currentTime, currentYear, currentMonth, today, agoTime, formatTime }
