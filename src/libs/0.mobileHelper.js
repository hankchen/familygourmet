/* 手機版畫面的 helper */
import store from 'src/store'
import router from 'src/router'
import {deviseFunction} from 'src/libs/deviseHelper.js'


// 驗證是否登入，若未登入則導向 /login
function validateLogin() {
  const isLogin = store.getters['mobile/isLogin']
  if (!isLogin) router.push('/login')
}

// (藉由殼 API) 存入登入資訊
function deviseSetLoginInfo(payload) {
  // console.log('mobileHelper ===> deviseSetLoginInfo')
  // payload sample: {type: 'set', data: res.data, rememberMe: true/false} , {type: 'reset'}

  // 預設值
  let data = {
    "EnterpriseID": 'jinhercloud',
    "UserCode": "",
    "UserName": "",
    "UserMac": "",
    "UserTokenkey": ""
  }

  // 存入登入資料
  if (payload.type === 'set' && payload.rememberMe === true) {
    data['EnterpriseID'] = payload.data.EnterPriseID
    data['UserCode']     = payload.data.UserCode
    data['UserName']     = payload.data.UserName
    data['UserMac']      = payload.data.mac
    data['UserTokenkey'] = payload.data.Tokenkey

    // 存入殼的推播設定
    deviseFunction('Do_Register', `{"MembersName":"${payload.data.UserName}", "Account":"${payload.data.UserCode}"}`, '')
  }
  // 寫入要存的 key 設定
  data['_KEY'] = "EnterpriseID,UserCode,UserName,UserMac,UserTokenkey"

  // 執行存入殼裡
  deviseFunction('SetSPS', JSON.stringify(data), '')
}

export { validateLogin, deviseSetLoginInfo }
