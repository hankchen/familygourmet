  // (公開) 取 currentApp 的公開資料 - HomeCoupon頁
  async fetchAppPublicDataCouponWuhu({dispatch}){
    // 自動領券(要在fetchMemberTicketListDataWuhu之前且要await)
    //console.log("xxxx>>1/cardCoupon我的優惠券");
    await dispatch('fetchGetDrawAutoTicketWuhu')

    // 取優惠卷列表資訊
    dispatch('fetchMemberTicketListDataWuhu')	// isActiveTab==1, 優惠券-可使用
    // 取會員已轉贈的優惠卷資訊
    dispatch('fetchMemberGaveTicketListDataWuhu')	// isActiveTab==3, 優惠券-已轉贈
    // 取尚未能使用的優惠卷資料
    dispatch('fetchMemberNotAvailableTicketsWuhu')	// isActiveTab==2, 優惠券-未生效
  },

  // 自動領券(單純告訴後端,要開始領了的觸發act...
  async fetchGetDrawAutoTicketWuhu({state, getters}){
    if (!getters.isLogin)		return
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({ act: "getdrawautoticket" })
    const data = await fetchData({url, body})
    if (data.error) return

    return new Promise(resolve => {resolve(true)})
  },

  // 取優惠卷列表資訊 - 五互,fig用此	// isActiveTab==1, 優惠券-可使用
  async fetchMemberTicketListDataWuhu({state, commit, getters}){
    if (!getters.isLogin)		return
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({ act: "GetVipTicket" })
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberTicketListDataWuhu', data)
  },
  // 存入優惠卷列表資訊 - 五互
  setMemberTicketListDataWuhu(state, data) {
    state.baseInfo.memberData.ticketList = data
  },

  // 取尚未能使用的優惠卷資料 - 五互	// isActiveTab==2, 優惠券-未生效
  async fetchMemberNotAvailableTicketsWuhu({state, commit, getters}){
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({ act: "NotTodayTicketList" })
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberNotAvailableTicketsWuhu', data)
  },
  setMemberNotAvailableTicketsWuhu(state, data) {
    state.baseInfo.memberData.notAvailableTickets = data
  },

  // 取已贈送的優惠卷列表資訊 - 五互	// isActiveTab==3, 優惠券-已轉贈
  async fetchMemberGaveTicketListDataWuhu({state, commit, getters}){
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({ act: "GetGaveTicketRecord" })
    const data = await fetchData({url, body})

    if (data.error) return

    commit('setMemberGaveTicketListDataWuhu', data)
  },
  setMemberGaveTicketListDataWuhu(state, data) {
    state.baseInfo.memberData.gaveTicketList = data
  },



