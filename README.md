# member-app

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


---


## 使用解說:

用 source tree 打開此專案, 點擊右上角的『Terminal』，打開終端機視窗

輸入 `yarn; yarn serve` 安裝 & 執行開發環境


## 系統參數：站台的切換

複製根目錄的 .env , 並將檔名存成 .env.local

```
# 設定網站參數 (目前可用的為: kakar , wuhulife)
VUE_APP_SITE=kakar    # 將站台設成 卡+ (預設)
VUE_APP_SITE=wuhulife # 將站台設成 龍海
```


## 打包並更新至正式站/測試站

複製以下的網址並執行即可

- 卡+ (正式站): https://jenkins.jinher.com.tw/generic-webhook-trigger/invoke?token=kakar
- 卡+ (測試站): https://jenkins.jinher.com.tw/generic-webhook-trigger/invoke?token=kakar-s

- 龍海 (正式站): https://jenkins.jinher.com.tw/generic-webhook-trigger/invoke?token=wuhulife
- 龍海 (測試站): https://jenkins.jinher.com.tw/generic-webhook-trigger/invoke?token=wuhulife-s

